<?php

global $lang;

$lang = basename($_SERVER['PHP_SELF'],'.php');

$data1 = file_get_contents ("../shared/shared.json", FILE_USE_INCLUDE_PATH);
$json1 = json_decode($data1,true);

$data2 = file_get_contents ("translations.json", FILE_USE_INCLUDE_PATH);
$json2 = json_decode($data2,true);
//print_r($json2);

include ('../shared/page-start.php');

?>

      <table align="center" border="0" cellpadding="0" cellspacing="0" class="wrapper-body" width="100%" style="" bgcolor="#e6e6e6">
         <tr>
            <td align="center">
               <?php
               include ('../shared/logo-strip.php');
               ?>
               <table align="center" border="0" cellpadding="0" cellspacing="0" class="wrapper-width wrapper-content" style="width: 640px;" bgcolor="#ffffff">
                  <tr>
                     <td>
                        <table border="0" cellpadding="0" cellspacing="0" class="full-width" align="center" style="width: 100%;">
                           <tr>
                              <td align="center" valign="top">
                                 <table border="0" cellpadding="20" cellspacing="0" class="contentWide-width" style="width: 100%; padding: 0 20px;">
                                    <tr>
                                       <td align="center" class="mobile-stack" valign="top">
                                          <?php
                                          $icon = true;
                                          $icon_type = 'padlock';
                                          $titleText = $json2["{$lang}"]['titleText'];
                                          include ('../shared/h1-title.php');

                                          $noPadding = true;
                                          include ('../shared/salutation.php');
                                          $noPadding = false;
                                          $paragraphText = $json2["{$lang}"]['previewText'];
                                          include ('../shared/paragraph.php');
                                          ?>
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                           <tr>
                              <td align="center" valign="top">
                                 <table border="0" cellpadding="0" cellspacing="0" class="contentWide-width" style="width: 100%; padding: 0 20px;">
                                    <tr>
                                       <td align="center" class="mobile-stack" valign="top">
                                          <?php
                                          $buttonLink = '{{baseUrl}}/reset-password/{{resetKey}}';
                                          $buttonWidth = 71;
                                          $buttonText = $json2["{$lang}"]['buttonText'];
                                          include ('../shared/button-table.php');
                                          ?>
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                           <tr>
                              <td align="center" valign="top">
                                 <table border="0" cellpadding="0" cellspacing="0" class="contentWide-width" style="width: 100%; padding: 0 20px;">
                                    <tr>
                                       <td align="center" class="mobile-stack" valign="top">
                                          <?php
                                          $noPadding = false;
                                          $paragraphText = $json2["{$lang}"]['introductionText1'];
                                          include ('../shared/paragraph.php');

                                          $noPadding = false;
                                          $paragraphText = $json2["{$lang}"]['introductionText2'];
                                          include ('../shared/paragraph.php');
                                          ?>
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                           <tr>
                              <td align="center" valign="top">
                                 <table border="0" cellpadding="0" cellspacing="0" class="contentWide-width" style="width: 100%; padding: 0 20px;">
                                    <tr>
                                       <td align="center" class="mobile-stack" valign="top">
                                          <?php 
                                          $noPadding = true;
                                          $thanks = true;
                                          include ('../shared/signoff.php');
                                          ?>
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                        </table>
                     </td>
                  </tr>
               </table>
               <?php
               include ('../shared/footer.php');
               ?>
            </td>
         </tr>
      </table>
<?php
include ('../shared/page-end.php');
?>