<?php

global $lang;

$lang = basename($_SERVER['PHP_SELF'],'.php');

$data1 = file_get_contents ("../shared/shared.json", FILE_USE_INCLUDE_PATH);
$json1 = json_decode($data1,true);

$data2 = file_get_contents ("translations.json", FILE_USE_INCLUDE_PATH);
$json2 = json_decode($data2,true);

include ('../shared/tableWARE/transactional_header.php');

include ('../shared/tableWARE/container_start.php');

$alignCenter = true;
include ('../shared/tableWARE/610_logo.php');
	
$alignLeft = false;
$icon = true;
$iconType = 'lexis';
$heroColor = "red";
$titleText = $json2["{$lang}"]['titleText'];
$subtitleText = $json2["{$lang}"]['subtitleText'];
$subtitle = true;
include ('../shared/tableWARE/hero_text.php');

include ('../shared/tableWARE/spacer_row_20.php');

$alignLeft = true;
$paragraphText = $json2["{$lang}"]['introductionText1'];
include ('../shared/tableWARE/text_block.php');

$paragraphText = $json2["{$lang}"]['introductionText2'];
include ('../shared/tableWARE/text_block.php');

$linkURL = 'https://rcmarketplace.zendesk.com/hc/en-us/articles/360016366913-Security-Check';
$paragraphText = $json2["{$lang}"]['introductionText3'];
include ('../shared/tableWARE/link_block.php');

$paragraphText = $json2["{$lang}"]['introductionText4'];
include ('../shared/tableWARE/text_block.php');

include ('../shared/tableWARE/spacer_row_20.php');

$alignLeft = false;
$hasPadding = true;
$ctaColor = 'red';
$textBold = false;
$noUrchin = false;
$btnText = $json2["{$lang}"]['buttonText'];
$btnLink = 'https://marketplace.rentalcars.com/#/sanction-screening';
$btnTitle = $json2["{$lang}"]['buttonText'];
include ('../shared/tableWARE/cta.php');


include ('../shared/tableWARE/sign_off.php');

include ('../shared/tableWARE/spacer_row_20.php');


include ('../shared/tableWARE/footer.php');

include ('../shared/tableWARE/container_end.php');

?>