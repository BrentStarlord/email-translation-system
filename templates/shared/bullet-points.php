<table align="<?php echo (isset($alignLeft) && $alignLeft == true ? 'left' : 'center'); ?>" border="0" cellspacing="0" class="contentWide-width full-width" style="width: 100%; padding: <?php echo ((isset($noPadding) && $noPadding == true) || (isset($noVerticalPadding) && $noVerticalPadding == true) ? '0px' : '20px'); ?> <?php echo (isset($noPadding) && $noPadding == true ? '0' : '20px'); ?>;">
  <tr>
    <td valign="top">
      <table align="center" border="0" cellpadding="10" cellspacing="0" class="contentWide-width full-width" style="width: 100%;">
        <?php if (isset($titleText) && $titleText != false ) { ?>
        <tr>
          <td align="center" valign="top" class="text-h2 content-row content-center" style="font-family: Verdana, Arial, sans-serif; font-size: 24px; border-bottom-width: 10px; border-bottom-color: #ffffff; border-bottom-style: solid;">
            <?php echo $titleText; ?>
          </td>
        </tr>
        <?php } ?>

        <?php if (isset($introText) && $introText != false ) { ?>
        <tr>
          <td align="<?php echo (isset($alignLeft) && $alignLeft == true ? 'left' : 'center'); ?>" class="text-h4 content-left" style="font-family: Verdana, Arial, sans-serif; font-size: 16px; line-height: 24px;"><?php echo $introText; ?></td>
        </tr>
        <?php } ?>
        <tr>
          <td valign="middle">
            <table align="center" border="0" cellpadding="0" cellspacing="0" class="contentWide-width full-width" style="width: 100%;">
              <?php for ($i = 1; $i <= $number; $i++) { ?>
              <tr class="mobile-stack">
                <td class="content-row spacerLeft text-bullet" style="width: 13px; font-size: 25px; font-family: times new roman; line-height: 36px; color: #0e94f7; border-left-width: 10px; border-left-color: #ffffff; border-left-style: solid; border-bottom-width: <?php echo (isset($noVerticalPadding) && $noVerticalPadding == true ? '0px' : '20px'); ?>; border-bottom-color: #ffffff; border-bottom-style: solid; text-align: center;" align="center" valign="top">•</td>
                <td class="content-row spacerLeft text-h4" style="font-family: Verdana, Arial, sans-serif; font-size: 16px; line-height: 24px; border-left-width: 10px; border-left-color: #ffffff; border-left-style: solid; border-bottom-width: <?php echo (isset($noVerticalPadding) && $noVerticalPadding == true ? '0px' : '20px'); ?>; border-bottom-color: #ffffff; border-bottom-style: solid;" align="left"><?php echo ${"text$i"}; ?></td>
              </tr>
              <?php
              }
              ?>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
