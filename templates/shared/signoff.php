
  <table align="center" border="0" cellpadding="20" cellspacing="0" class="spacerBottom" style="border-bottom-width: 0px; border-bottom-color: #ffffff; border-bottom-style: solid;">
    <tr>
      <td>
        <table align="center" border="0" cellpadding="10" cellspacing="0">
          <tr>
            <?php if ( isset($thanks) ) { ?>
              <td class="text-h4 content-center" style="font-family: Verdana, Arial, sans-serif; font-size: 16px; line-height: 24px;" align="center"><?php echo $json1["{$lang}"]['footerThanksText'];?></td>
            <?php } else if ( isset($seeyou) ) { ?>
              <td class="text-h4 content-center" style="font-family: Verdana, Arial, sans-serif; font-size: 16px; line-height: 24px;" align="center"><?php echo $json1["{$lang}"]['footerSeeYouText'];?></td>
            <?php } else {?>
              <td class="text-h4 content-center" style="font-family: Verdana, Arial, sans-serif; font-size: 16px; line-height: 24px;" align="center"><?php echo $json1["{$lang}"]['footerThanksText'];?></td>
            <?php }?>
          </tr>
          <tr>
            <td class="text-h4 content-center" style="font-family: Verdana, Arial, sans-serif; font-size: 16px; line-height: 24px;" align="center"><?php echo $json1["{$lang}"]['footerTeamText'];?></td>
          </tr>
        </table>
      </td>
    </tr>
  </table>