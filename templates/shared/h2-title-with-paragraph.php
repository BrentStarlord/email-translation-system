
<table align="<?php echo (isset($alignLeft) && $alignLeft == true ? 'left' : 'center'); ?>" border="0" cellpadding="<?php echo (isset($noPadding) && $noPadding == true ? '0' : '20'); ?>" cellspacing="0">
  <tr>
    <td align="<?php echo (isset($alignLeft) && $alignLeft == true ? 'left' : 'center'); ?>" valign="top" class="text-h2 content-row content-<?php echo (isset($alignLeft) && $alignLeft == true ? 'left' : 'center'); ?>" style="font-family: Verdana, Arial, sans-serif; font-size: 24px; border-bottom-width: 20px; border-bottom-color: #ffffff; border-bottom-style: solid;">
      <?php echo $titleText; ?>
    </td>
  </tr>
  <tr>
    <td class="text-normal" style="font-family: Verdana, Arial, sans-serif; font-size: 14px;"> <?php echo $paragraphText1; ?></td>
  </tr>
  <tr>
    <td class="spacerBottom-narrow text-normal" style="font-family: Verdana, Arial, sans-serif; font-size: 14px; padding-bottom: <?php echo (isset($noPadding) && $noPadding == true ? '0px' : '10px'); ?>;"><?php echo $paragraphText2; ?></td>
  </tr>
</table>