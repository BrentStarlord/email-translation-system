<table align="<?php echo (isset($alignLeft) && $alignLeft == true ? 'left' : 'center'); ?>" border="0" cellpadding="<?php echo (isset($noPadding) && $noPadding == true ? '0' : '20'); ?>" cellspacing="0">
  <tr>
    <td align="<?php echo (isset($alignLeft) && $alignLeft == 'left' ? 'left' : 'center'); ?>" class="text-h4 content-<?php echo (isset($alignLeft) && $alignLeft == true ? 'left' : 'center'); ?>" style="font-family: Verdana, Arial, sans-serif; font-size: 16px; line-height: 24px;"><?php echo $paragraphText; ?></td>
  </tr>
</table>