<tr>
  <td>
    <table border="0" cellpadding="0" cellspacing="0" class="contentWide-width" style="padding: 0 20px;">
      <tbody>
        <?php if ( isset($introText) && $introText != false ) { ?>
        <tr>
          <td align="left" valign="top" class="text-h4 spacerTop content-row" style="font-family: Verdana, Arial, sans-serif; font-size: 16px; line-height: 24px; border-top-width: 10px; border-top-color: #ffffff; border-top-style: solid; border-bottom-width: 20px; border-bottom-color: #ffffff; border-bottom-style: solid;">
            Discover the Marketplace Dashboard, where you can:
          </td>
        </tr>
        <?php
        }
        ?>
        <tr>
        <?php for ($i = 1; $i <= $number; $i++) { ?>
          <td align="center" valign="top" width="50%" class="contentWide-width content-stack" style="padding: 0 20px;">
            <table align="center" border="0" cellpadding="2" cellspacing="0" class="full-width" style="width: 100%;">
               <tbody>
                <tr>
                  <td align="center" valign="top" class="spacerTop spacerBottom" style="border-top-width: 10px; border-top-color: #ffffff; border-top-style: solid; border-bottom-width: 20px; border-bottom-color: #ffffff; border-bottom-style: solid;">
                    <img class="retinaReady" alt="" src="<?php echo ${"icon_$i"}; ?>" width="<?php echo ${"width_$i"}; ?>" style="max-width: <?php echo ${"width_$i"}; ?>px; display: block; width: 100%; font-family: sans-serif; border: 0px; color: #ffffff; font-size: 11px;" border="0">
                  </td>
                </tr>
                <tr>
                  <td align="center" class="text-h3 content-center" style="font-family: Verdana, Arial, sans-serif; font-size: 16px; font-weight: bold;"><?php echo ${"title_$i"}; ?></td>
                </tr>
                <tr>
                  <td align="center" class="text-normal content-row content-center" style="font-family: Verdana, Arial, sans-serif; font-size: 14px; border-bottom-width: 20px; border-bottom-color: #ffffff; border-bottom-style: solid;"><?php echo ${"text_$i"}; ?></td>
                </tr>
              </tbody>
            </table>
          </td>
        <?php
        }
        ?>
        </tr>
      </tbody>
    </table>
  </td>
</tr>