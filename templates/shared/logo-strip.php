<div class="previewText" style="display: none; font-size: 1px; color: #333333; line-height: 1px; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">
   <?php echo $json2["{$lang}"]['previewText'];?>
</div>
<?php if (isset($viewInBrowser) ) { ?>
<table align="center" border="0" cellpadding="20" cellspacing="0" class="wrapper-width" style="width: 640px;">
   <tbody>
      <tr>
         <td align="center" class="text-small" style="font-family: Verdana, Arial, sans-serif; font-size: 11px;"><?php echo $json1["{$lang}"]['viewInBrowserText1'];?> <a href="[Weblink]"><span class="text-link" style="text-decoration: underline; color: #000000;"><?php echo $json1["{$lang}"]['viewInBrowserText2'];?></span></a> </td>
      </tr>
   </tbody>
</table>
<?php } ?>
<table align="<?php echo (isset($alignCenter) && $alignCenter == true) ? 'center' : 'left' ?>" border="0" cellpadding="0" cellspacing="0" class="wrapper-width wrapper-content" style="width: 640px;" bgcolor="#ffffff">
   <tr>
      <td>
         <table border="0" cellpadding="20" cellspacing="0" class="full-width" align="<?php (isset($alignCenter) && $alignCenter == true) ? 'center' : 'left' ?>" style="width: 100%;">
            <tr>
               <td align="<?php echo (isset($alignCenter) && $alignCenter == true) ? 'center' : 'left' ?>" valign="top">
                  <table border="0" cellpadding="0" cellspacing="0" class="">
                     <tr>
                        <td align="<?php echo (isset($alignCenter) && $alignCenter == true) ? 'center' : 'left' ?>" class="mobile-stack" valign="top"><a title="<?php echo $json1["{$lang}"]['headLinkTitle'];?>" target="_blank" href="{{baseUrl}}" class="text-cta" style="font-family: Verdana, Arial, sans-serif; font-size: 14px; text-decoration: none; color: #ffffff;"> <img class="retinaReady" alt="Marketplace by Rentalcars.com" src="https://marketing-image-production.s3.amazonaws.com/uploads/e50393ab2b1b62bbf2f786ba2b270a50c6daeb5c7d550be9f101974258319cf6f6e60210ae5adef3f848401608b5f099ffe9b554f6bd6a11f57644607554102d.png" width="180" style="max-width: 180px; display: block; width: 100%; font-family: sans-serif; border: 0px; color: #ffffff; font-size: 11px;" border="0"></a>   </td>
                     </tr>
                  </table>
               </td>
            </tr>
         </table>
      </td>
   </tr>
</table>