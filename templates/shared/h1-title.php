
<table border="0" cellpadding="<?php echo (isset($noPadding) ? '0' : '20'); ?>" cellspacing="0" class="full-width" align="center" style="width: 100%;">
  <tr>
    <td align="center" valign="top">
      <table border="0" cellpadding="0" cellspacing="0" class="">
        <?php 
        if ( isset($icon) && $icon == true ) {
        ?>
        <tr>
          <td align="center" valign="top">
            <table align="center" valign="middle" border="0" cellpadding="10" cellspacing="0">
              <tr>
                <td align="center" valign="top">
                  <?php if ($icon_type == 'padlock') { ?>
                  <img class="retinaReady spacerTop" alt="" src="https://marketing-image-production.s3.amazonaws.com/uploads/88e91897fbb2ec92fcfe8d40fb2ae6904fab177d363d18a9fa1066cf595f4e95f01e8724a981ce5f73ae257210c8bc9ecd9798d9eb34fd27f19f534d7d7eb718.png" width="44" style="max-width: 44px; display: block; width: 100%; font-family: sans-serif; color: #ffffff; font-size: 11px; border-top-color: #ffffff; border-top-style: solid;" border="0">
                  <?php } ?>
                  <?php if ($icon_type == 'smile') { ?>
                  <img class="retinaReady spacerTop" alt="" src="https://marketing-image-production.s3.amazonaws.com/uploads/1b3cadc44926515343636183647bef713c9675873ecae9324e38401d2b6623c974d80dda1e9ccc717bc053a8803c0a76d30d602c018a19fb7e63184d8cc4fa9a.png" width="60" style="max-width: 60px; display: block; width: 100%; font-family: sans-serif; color: #ffffff; font-size: 11px; border-top-color: #ffffff; border-top-style: solid;" border="0">
                  <?php } ?>

                  <?php if ($icon_type == 'rfp-submitted') { ?>
                  <img class="retinaReady spacerTop" alt="" src="https://marketing-image-production.s3.amazonaws.com/uploads/0e2648ad2ad0ef3adf7c0102a0a9c1bc4c26a4fbbcc997b0ea50430ce899ae2ecb9d09b971329988af82f9a8f4e5baea7649f70b0427299decc6b987df94fa21.png" width="44" style="max-width: 44px; display: block; width: 100%; font-family: sans-serif; color: #ffffff; font-size: 11px; border-top-color: #ffffff; border-top-style: solid;" border="0">
                  <?php } ?>

                  <?php if ($icon_type == 'rfp-released') { ?>
                  <img class="retinaReady spacerTop" alt="" src="https://marketing-image-production.s3.amazonaws.com/uploads/1079a32c64326db42f6be4c80b4cd98dd85d21f0975ca1d99eb8e1a3ca6315a2c92eec4709a9fd5c61f8b1af8373adc2c7b4e47319c6aeac4dd644c5c224f14f.png" width="44" style="max-width: 44px; display: block; width: 100%; font-family: sans-serif; color: #ffffff; font-size: 11px; border-top-color: #ffffff; border-top-style: solid;" border="0">
                  <?php } ?>

                  <?php if ($icon_type == 'res-amended') { ?>
                  <img class="retinaReady spacerTop" alt="" src="https://marketing-image-production.s3.amazonaws.com/uploads/e1d00bed56437d4dd81b0243d041494f3d547b051d815855058591c29ea059a3b7ae676223c10a8bc0f4c7f1f0cc6ca5da7586e8e13ca93c8544089c4005c795.png" width="60" style="max-width: 60px; display: block; width: 100%; font-family: sans-serif; color: #ffffff; font-size: 11px; border-top-color: #ffffff; border-top-style: solid;" border="0">
                  <?php } ?>

                  <?php if ($icon_type == 'res-canceled') { ?>
                  <img class="retinaReady spacerTop" alt="" src="https://marketing-image-production.s3.amazonaws.com/uploads/164e050287758eac9bfb1da97fcbd6046de3322dade768fb80770af6b5c01610cc8c94752e8705a4a7998e20aef886d3285c7f338d2e258fd7da08399505b055.png" width="60" style="max-width: 60px; display: block; width: 100%; font-family: sans-serif; color: #ffffff; font-size: 11px; border-top-color: #ffffff; border-top-style: solid;" border="0">
                  <?php } ?>


                  <?php if ($icon_type == 'res-created') { ?>
                  <img class="retinaReady spacerTop" alt="" src="https://marketing-image-production.s3.amazonaws.com/uploads/10f00bd88bc94fce053bb6597e29826f09a9890a6b75bf6f391af081b7a2a2acf50c25bd7dec0be5320083d497493e7aa658e084ab742bae666463376489703f.png" width="60" style="max-width: 60px; display: block; width: 100%; font-family: sans-serif; color: #ffffff; font-size: 11px; border-top-color: #ffffff; border-top-style: solid;" border="0">
                  <?php } ?>

                  <?php if ($icon_type == 'graph') { ?>
                  <img class="retinaReady spacerTop" alt="" src="https://www.rentalcars.com/marketplace/images/why-bookings@2x.png" width="70" style="max-width: 70px; display: block; width: 100%; font-family: sans-serif; color: #ffffff; font-size: 11px; border-top-color: #ffffff; border-top-style: solid;" border="0">
                  <?php } ?>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <?php 
        }

        if ( $titleText != '' ) {
        ?>
        <tr>
          <td align="center" valign="top">
            <table border="0" cellpadding="10" cellspacing="0" class="">
              <tr>
                <td align="center" valign="top" class="text-h1 content-center" style="font-family: Verdana, Arial, sans-serif; font-size: 32px; font-weight: bold;">
                  <?php echo $titleText; ?>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <?php 
        }
        ?>
      </table>
    </td>
  </tr>
</table>