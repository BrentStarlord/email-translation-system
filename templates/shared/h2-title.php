
<table align="<?php echo (isset($alignLeft) && $alignLeft == true ? 'left' : 'center'); ?>" border="0" cellpadding="<?php echo (isset($noPadding) && $noPadding == true ? '0' : '20'); ?>" cellspacing="0">
  <tr>
    <td align="<?php echo (isset($alignLeft) && $alignLeft == true ? 'left' : 'center'); ?>" valign="top" class="text-h2 content-row content-<?php echo (isset($alignLeft) && $alignLeft == true ? 'left' : 'center'); ?>" style="font-family: Verdana, Arial, sans-serif; font-size: 24px; border-bottom-width: 20px; border-bottom-color: #ffffff; border-bottom-style: solid;">
      <?php echo $titleText; ?>
    </td>
  </tr>
</table>