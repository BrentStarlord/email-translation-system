<table align="center" border="0" cellpadding="0" cellspacing="0" class="wrapper-width wrapper-content" style="width: 640px;" bgcolor="#ffffff">
  <tr>
    <td>
      <table border="0" cellpadding="20" cellspacing="0" class="full-width" align="center" style="width: 100%;">
        <tr>
          <td align="center" valign="top">
            <table border="0" cellpadding="0" cellspacing="0" class="">
              <tr>
                <td align="center" valign="top" class="text-h1 content-row content-center" style="font-family: Verdana, Arial, sans-serif; font-size: 32px; font-weight: bold; border-bottom-width: 20px; border-bottom-color: #ffffff; border-bottom-style: solid;"><?php echo $json2["{$lang}"]['titleText'];?></td>
              </tr>
              <tr>
                <td align="center" valign="top" class="text-h2 content-center" style="font-family: Verdana, Arial, sans-serif; font-size: 24px;"><?php echo $json2["{$lang}"]['subtitleText'];?></td>
                 <!-- <span class="desktop-nowrap" style="white-space: nowrap;">of Rentalcars.com customers</span> -->
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>