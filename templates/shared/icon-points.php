<table align="<?php echo (isset($alignLeft) && $alignLeft == true ? 'left' : 'center'); ?>" border="0" cellpadding="<?php echo (isset($noPadding) && $noPadding == true ? '0' : '20'); ?>" cellspacing="0" class="contentWide-width full-width" style="width: 100%;">
  <tr>
    <td valign="middle">
      <table align="center" border="0" cellpadding="10" cellspacing="0" class="contentWide-width full-width" style="width: 100%;">
        <?php if (isset($titleText) && $titleText != false ) { ?>
        <tr>
          <td align="center" valign="top" class="text-h2 content-row content-center" style="font-family: Verdana, Arial, sans-serif; font-size: 24px; border-bottom-width: 10px; border-bottom-color: #ffffff; border-bottom-style: solid;">
            <?php echo $titleText; ?>
          </td>
        </tr>
        <?php } ?>

        <?php if (isset($introText) && $introText != false ) { ?>
        <tr>
          <td align="<?php echo (isset($alignLeft) && $alignLeft == true ? 'left' : 'center'); ?>" class="text-h4 content-left" style="font-family: Verdana, Arial, sans-serif; font-size: 16px; line-height: 24px;"><?php echo $introText; ?></td>
        </tr>
        <?php } ?>
        <tr>
          <td valign="middle">
            <table align="center" border="0" cellpadding="0" cellspacing="0" class="contentWide-width full-width" style="width: 100%;">
              <?php for ($i = 1; $i <= $number; $i++) { ?>
              <tr class="mobile-stack">
                <td align="center" class="mobile-stack" valign="top" style="display: block !important;">
                  <table align="left" border="0" cellpadding="2" cellspacing="0" class="full-width content-row" style="width: 100%; border-bottom-width: 20px; border-bottom-color: #ffffff; border-bottom-style: solid; text-align: left;">
                    <tbody>
                      <tr>
                        <td align="left" valign="top" width="30" class="inlineIcon" style="padding-top: 5px;">
                          <img class="retinaReady" alt="" src="<?php echo ${"icon_$i"}; ?>" width="<?php echo ${"width_$i"}; ?>" style="max-width: <?php echo ${"width_$i"}; ?>px; display: block; width: 100%; font-family: sans-serif; border: 0px; color: #ffffff; font-size: 11px;" border="0">
                        </td>
                        <td>
                          <table align="left" border="0" cellpadding="2" cellspacing="0">
                            <tbody>
                              <tr>
                                <td class="text-h3" style="font-family: Verdana, Arial, sans-serif; font-size: 16px; font-weight: bold;"><?php echo ${"title_$i"}; ?></td>
                              </tr>
                              <tr>
                                <td class="text-normal" style="font-family: Verdana, Arial, sans-serif; font-size: 14px;"><?php echo ${"text_$i"}; ?></td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <?php
              }
              ?>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>