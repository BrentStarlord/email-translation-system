<?php
if (isset($hasLink) && $hasLink == true ) {
  $split = explode("|", $paragraphText);
}
?>
 <!-- $$$$$ Hero module starts -->
  <div>
    <table class="m-span10 tbl-610 parent m-margin-bottom <?php echo (isset($icon) && $icon == true ) ? 'hero-icon' : 'hero' ;?> <?php echo (isset($heroColor) ) ? 'make-' . $heroColor : '' ; ?> " align="center" width="610" border="0" cellspacing="0" cellpadding="0">
      <tbody>
        <tr>
          <td height="40" style="height: 40px; line-height: 40px; font-size: 40px; mso-line-height-rule: exactly;" colspan="3">&nbsp;</td>
        </tr>
        <tr>
          <td class="m-hide" width="30" style="width: 30px;"></td>
          <td align="<?php echo (isset($alignLeft) && $alignLeft == true ? 'left' : 'center'); ?>">

            <?php if (isset($icon) && $icon == true ) { ?>
            <table class="m-span10 m-padding" align="<?php echo (isset($alignLeft) && $alignLeft == true ? 'left' : 'center'); ?>" width="550" border="0" cellspacing="0" cellpadding="0" style="width: 550px;">
              <tbody>
                <tr>
                  <td valign="top" align="<?php echo (isset($alignLeft) && $alignLeft == true ? 'left' : 'center'); ?>">
                    <?php if ($iconType == 'padlock') { ?>
                    <img class="retinaReady spacerTop" alt="" src="https://marketing-image-production.s3.amazonaws.com/uploads/88e91897fbb2ec92fcfe8d40fb2ae6904fab177d363d18a9fa1066cf595f4e95f01e8724a981ce5f73ae257210c8bc9ecd9798d9eb34fd27f19f534d7d7eb718.png" width="44" style="max-width: 44px; display: block; width: 100%; font-family: sans-serif; color: #ffffff; font-size: 11px; border-top-color: #ffffff; border-top-style: solid;" border="0">
                    <?php } ?>
                    <?php if ($iconType == 'smile') { ?>
                    <img class="retinaReady spacerTop" alt="" src="https://marketing-image-production.s3.amazonaws.com/uploads/1b3cadc44926515343636183647bef713c9675873ecae9324e38401d2b6623c974d80dda1e9ccc717bc053a8803c0a76d30d602c018a19fb7e63184d8cc4fa9a.png" width="60" style="max-width: 60px; display: block; width: 100%; font-family: sans-serif; color: #ffffff; font-size: 11px; border-top-color: #ffffff; border-top-style: solid;" border="0">
                    <?php } ?>

                    <?php if ($iconType == 'rfp-submitted') { ?>
                    <img class="retinaReady spacerTop" alt="" src="https://marketing-image-production.s3.amazonaws.com/uploads/0e2648ad2ad0ef3adf7c0102a0a9c1bc4c26a4fbbcc997b0ea50430ce899ae2ecb9d09b971329988af82f9a8f4e5baea7649f70b0427299decc6b987df94fa21.png" width="44" style="max-width: 44px; display: block; width: 100%; font-family: sans-serif; color: #ffffff; font-size: 11px; border-top-color: #ffffff; border-top-style: solid;" border="0">
                    <?php } ?>

                    <?php if ($iconType == 'rfp-released') { ?>
                    <img class="retinaReady spacerTop" alt="" src="https://marketing-image-production.s3.amazonaws.com/uploads/1079a32c64326db42f6be4c80b4cd98dd85d21f0975ca1d99eb8e1a3ca6315a2c92eec4709a9fd5c61f8b1af8373adc2c7b4e47319c6aeac4dd644c5c224f14f.png" width="44" style="max-width: 44px; display: block; width: 100%; font-family: sans-serif; color: #ffffff; font-size: 11px; border-top-color: #ffffff; border-top-style: solid;" border="0">
                    <?php } ?>

                    <?php if ($iconType == 'res-amended') { ?>
                    <img class="retinaReady spacerTop" alt="" src="https://rcmp-marketing-prod.s3.amazonaws.com/images/email/orange-car-t.png" width="60" style="max-width: 60px; display: block; width: 100%; font-family: sans-serif; color: #ffffff; font-size: 11px; border-top-color: #ffffff; border-top-style: solid;" border="0">
                    <?php } ?>

                    <?php if ($iconType == 'res-canceled') { ?>
                    <img class="retinaReady spacerTop" alt="" src="https://rcmp-marketing-prod.s3.amazonaws.com/images/email/red-car-t.png" width="60" style="max-width: 60px; display: block; width: 100%; font-family: sans-serif; color: #ffffff; font-size: 11px; border-top-color: #ffffff; border-top-style: solid;" border="0">
                    <?php } ?>

                    <?php if ($iconType == 'res-created') { ?>
                    <img class="retinaReady spacerTop" alt="" src="https://rcmp-marketing-prod.s3.amazonaws.com/images/email/green-car-t.png" width="60" style="max-width: 60px; display: block; width: 100%; font-family: sans-serif; color: #ffffff; font-size: 11px; border-top-color: #ffffff; border-top-style: solid;" border="0">
                    <?php } ?>

                    <?php if ($iconType == 'birthday') { ?>
                    <img class="retinaReady spacerTop" alt="" src="https://rcmp-marketing-prod.s3.amazonaws.com/images/email/birthday-cake.png" width="60" style="max-width: 60px; display: block; width: 100%; font-family: sans-serif; color: #ffffff; font-size: 11px; border-top-color: #ffffff; border-top-style: solid;" border="0">
                    <?php } ?>

                    <?php if ($iconType == 'holidays') { ?>
                    <img class="retinaReady spacerTop" alt="" src="https://rcmp-marketing-prod.s3.amazonaws.com/images/email/holidays-t.png" width="70" style="max-width: 70px; display: block; width: 100%; font-family: sans-serif; color: #ffffff; font-size: 11px; border-top-color: #ffffff; border-top-style: solid;" border="0">
                    <?php } ?>

                    <?php if ($iconType == 'graph') { ?>
                    <img class="retinaReady spacerTop" alt="" src="https://www.rentalcars.com/marketplace/images/why-bookings@2x.png" width="70" style="max-width: 70px; display: block; width: 100%; font-family: sans-serif; color: #ffffff; font-size: 11px; border-top-color: #ffffff; border-top-style: solid;" border="0">
                    <?php } ?>

                    <?php if ($iconType == 'add-location') { ?>
                    <img class="retinaReady spacerTop" alt="" src="https://rcmp-marketing-prod.s3.amazonaws.com/images/email/add-location.png" width="70" style="max-width: 70px; display: block; width: 100%; font-family: sans-serif; color: #ffffff; font-size: 11px; border-top-color: #ffffff; border-top-style: solid;" border="0">
                    <?php } ?>

                    <?php if ($iconType == 'lexis') { ?>
                    <img class="retinaReady spacerTop" alt="" src="https://rcmp-marketing-prod.s3.amazonaws.com/images/email/security-screening.png" width="70" style="max-width: 70px; display: block; width: 100%; font-family: sans-serif; color: #ffffff; font-size: 11px; border-top-color: #ffffff; border-top-style: solid;" border="0">
                    <?php } ?>

                    <?php if ($iconType == 'discounts') { ?>
                    <img class="retinaReady spacerTop" alt="" src="https://rcmp-marketing-prod.s3.amazonaws.com/images/email/discounts.png" width="70" style="max-width: 70px; display: block; width: 100%; font-family: sans-serif; color: #ffffff; font-size: 11px; border-top-color: #ffffff; border-top-style: solid;" border="0">
                    <?php } ?>

                    <?php if ($iconType == 'green-car') { ?>
                    <img class="retinaReady spacerTop" alt="" src="https://rcmp-marketing-prod.s3.amazonaws.com/images/email/green-car-t.png" width="70" style="max-width: 70px; display: block; width: 100%; font-family: sans-serif; color: #ffffff; font-size: 11px; border-top-color: #ffffff; border-top-style: solid;" border="0">
                    <?php } ?>
                  </td>
                </tr>
                <tr>
                  <td height="40" style="height: 20px; line-height: 20px; font-size: 20px; mso-line-height-rule: exactly;">&nbsp;</td>
                </tr>
              </tbody>
            </table>
            <?php } ?>
            <?php if ( isset($known) && $known == true) { ?>
            <table class="m-span10 m-padding" align="<?php echo (isset($alignLeft) && $alignLeft == true ? 'left' : 'center'); ?>" width="550" border="0" cellspacing="0" cellpadding="0" style="width: 550px;">
              <tbody>
                <tr>
                  <td valign="top" class="m-font-30" align="<?php echo (isset($alignLeft) && $alignLeft == true ? 'left' : 'center'); ?>" style="color: #ffffff; font-family: 'Open sans', Arial, sans-serif; text-decoration: none; text-transform: uppercase; font-size: 22px; font-weight: 400; text-align: center; line-height: 38px"><font face="'Open sans', Arial, sans-serif">Hello [%first_name | Valued%] [%last_name | Supplier%]</font>
                  </td>
                </tr>
              </tbody>
            </table>
            <?php } ?>
            <table class="m-span10 m-padding" align="<?php echo (isset($alignLeft) && $alignLeft == true ? 'left' : 'center'); ?>" width="550" border="0" cellspacing="0" cellpadding="0" style="width: 550px;">
              <tbody>
                <tr>
                  <?php if (isset($hasLink) && $hasLink == true ) { ?>
                    <font face="'Open sans', Arial, sans-serif">
                      <td valign="top" class="m-font-30" align="<?php echo (isset($alignLeft) && $alignLeft == true ? 'left' : 'center'); ?>" style="font-family:'Open sans', Arial, sans-serif; text-decoration: none; font-size: 32px; font-weight:bold; text-align:<?php echo (isset($alignLeft) && $alignLeft == true ? 'left' : 'center'); ?>; line-height: 38px;"><?php echo $split[0]; ?><a href="<?php echo ( isset($linkURL) ) ? $linkURL : '#'; echo $json2["{$lang}"]['urchin']; ?>&utm_content=link" target="_blank" class="text-link" style="white-space: nowrap; text-decoration: none; color: <?php echo (isset($heroColor) && $heroColor == 'green' ) ? '#5cb85c' : '#ffffff' ; ?>; font-weight: bold;"><?php echo $split[1]; ?></a><?php echo $split[2]; ?></td>
                    </font>
                  <?php } else { ?>
                    <td valign="top" class="m-font-30" align="<?php echo (isset($alignLeft) && $alignLeft == true ? 'left' : 'center'); ?>" style="font-family:'Open sans', Arial, sans-serif; text-decoration: none; font-size: 32px; font-weight:bold; text-align:<?php echo (isset($alignLeft) && $alignLeft == true ? 'left' : 'center'); ?>; line-height: 38px;"><font face="'Open sans', Arial, sans-serif"><?php echo $titleText; ?></font></td>
                  <?php } ?>
                </tr>
              </tbody>
            </table>
            <?php if (isset($subtitleText) ) { ?>
            <table class="m-span9" align="<?php echo (isset($alignLeft) && $alignLeft == true ? 'left' : 'center'); ?>" width="550" border="0" cellspacing="0" cellpadding="0" style="width: 550px;">
              <tbody>
                <tr>
                  <td height="20" style="height: 20px; line-height: 20px; font-size: 20px; mso-line-height-rule: exactly;" colspan="1">&nbsp;</td>
                </tr>
                <tr>
                  <td class="m-font-20" align="<?php echo (isset($alignLeft) && $alignLeft == true ? 'left' : 'center'); ?>" style="font-family:'Open sans', Arial, sans-serif; text-decoration: none; font-size: 24px; font-weight:300; text-align:<?php echo (isset($alignLeft) && $alignLeft == true ? 'left' : 'center'); ?>;"><font face="'Open sans', Arial, sans-serif">
                    <?php echo $subtitleText; ?>
                  </font></td>
                </tr>
              </tbody>
            </table>
            <?php } ?>
            <table class="m-span9" align="<?php echo (isset($alignLeft) && $alignLeft == true ? 'left' : 'center'); ?>" width="550" border="0" cellspacing="0" cellpadding="0" style="width: 550px;">
              <tbody>
                <tr>
                  <td height="20" style="height: 20px; line-height: 20px; font-size: 20px; mso-line-height-rule: exactly;" colspan="1">&nbsp;</td>
                </tr>
                <tr>
                  <td>
                    <?php if (isset($cta) && $cta == true) { ?>
                    <table class="m-span10" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td class="m-span10" align="<?php echo (isset($alignLeft) && $alignLeft == true ? 'left' : 'center'); ?>" style="border-radius: 4px; height: 45px;" bgcolor="#006092">
                          <a href="#" target="_blank" style="border-radius: 4px; font-size: 22px; font-family: 'Open Sans', Arial, sans-serif; color: #006092; text-decoration: none; border-radius: px; padding: 5px 25px; display: inline-block; font-weight: bold;">
                                <?php echo $subtitleText; ?></a>
                        </td>
                      </tr>
                    </table>
                    <?php } ?>
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
          <td class="m-hide" width="20" style="width: 20px;"></td>
        </tr>
        <?php if (!isset($noVerticalMargin) || $noVerticalMargin == false) { ?>
        <tr>
          <td height="30" style="height: 30px; line-height: 30px; font-size: 30px; mso-line-height-rule: exactly;" colspan="3">&nbsp;</td>
        </tr>
        <?php } ?>
      </tbody>
    </table>
    <table class="m-hide parent" align="<?php echo (isset($alignLeft) && $alignLeft == true ? 'left' : 'center'); ?>" width="610" border="0" cellspacing="0" cellpadding="0" style="width: 610px;">
      <tbody>
        <tr>
          <td height="20" style="height: 20px; line-height: 20px; font-size: 20px; mso-line-height-rule: exactly;" colspan="1">&nbsp;</td>
        </tr>
      </tbody>
    </table>
  </div>
  <!-- $$$$$ Hero module ends -->
