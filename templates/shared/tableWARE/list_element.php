
<!-- $$$$$ list element starts -->
<li style="margin:0 0 1em; list-style:disc inside; mso-special-format:bullet;"><font face="'Open sans', Arial, sans-serif"><?php if ( isset( $listLink ) ){
	$split = explode("|", $listText);
	echo $split[0]; ?> <a href="<?php echo ( isset($linkURL) ) ? $linkURL : '#'; echo $json2["{$lang}"]['urchin']; ?>&utm_content=link" target="_blank" class="text-link" style="white-space: nowrap; text-decoration: none; color: #0e94f7; font-weight: bold;"><?php echo $split[1]; ?></a><?php echo $split[2]; ?>
<?php } else { ?>
<?php echo $listItemText; ?>
<?php } ?></font>
<?php
	if ( isset($subListNumber) && $subListNumber > 0) {
?>
	<<?php echo $listStyle; ?>  style="margin:1em 0; padding:0;">
<?php
	for ($x = 1; $x <= $subListNumber; $x++) {?>
		<li style="margin:0 0 1em 1em; list-style:circle inside; mso-special-format:bullet;"><font face="'Open sans', Arial, sans-serif"><?php echo ${'subListItemText_' . $x}; ?></font></li>
<?php
} 
?>
	</<?php echo $listStyle; ?>>
<?php
	}
?>
</li>

<!-- $$$$$ list element ends -->