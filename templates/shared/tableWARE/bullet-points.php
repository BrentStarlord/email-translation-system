<!-- $$$$$ bullet points start -->
<div>
  <table class="m-span10 tbl-610" bgcolor="#ffffff" align="<?php echo (isset($alignLeft) && $alignLeft == true ? 'left' : 'center'); ?>" width="610" border="0" cellspacing="0" cellpadding="0" style="width: 610px; background-color: #ffffff;">
    <tr>
      <td class="<?php echo (isset($noPadding) && $noPadding == true ? '' : 'spacer-40-w'); ?>" width="<?php echo (isset($noPadding) && $noPadding == true ? '0' : '40'); ?>">&nbsp;</td>
      <td>

      	<table align="center" border="0" cellpadding="0" cellspacing="0" class="contentWide-width full-width" style="width: 100%;">


          <?php if (isset($bulletTitle) && $bulletTitle != false ) { ?>
          <tr>
            <td class="h2 make-<?php echo (isset($h2Color)) ? $h2Color : 'standard' ; ?>" align="<?php echo (isset($alignLeft) && $alignLeft == true ? 'left' : 'center'); ?>" style="font-family:'Open sans', Arial, sans-serif; text-decoration: none; font-size: 20px; padding: 20px; font-weight:800; text-align:<?php echo (isset($alignLeft) && $alignLeft == true ? 'left' : 'center'); ?>;">
              <font face="'Open sans', Arial, sans-serif">
                <?php echo $bulletTitle; ?>
              </font>
            </td>
          </tr>
          <tr>
            <td height="40" class="spacer-40-h">&nbsp;</td>
          </tr>
          <?php } ?>

          <?php if (isset($bulletIntro) && $bulletIntro != false ) { ?>
          <tr>
            <?php if (isset($introLink) && $introLink == true) {
              $split = explode("|", $bulletIntro); ?>
            <td align="<?php echo (isset($alignLeft) && $alignLeft == true ? 'left' : 'center'); ?>" style="font-family:'Open sans', Arial, sans-serif; text-decoration: none; font-size: 16px; color: #212121; font-weight:<?php echo (isset($textBold) && $textBold == true ? '800' : '300'); ?>; text-align:<?php echo (isset($alignLeft) && $alignLeft == true ? 'left' : 'center'); ?>;">
              <font face="'Open sans', Arial, sans-serif">
                <?php echo $split[0]; ?> <a href="<?php echo ( isset($linkURL) ) ? $linkURL : '#'; echo ( isset($noUrchin) && $noUrchin == true ) ? '' : $json2["{$lang}"]['urchin'] . '&utm_content=link'; ?>" target="_blank" class="text-link" style="white-space: nowrap; text-decoration: none; color: #0e94f7; font-weight: bold;"><?php echo $split[1]; ?></a><?php echo $split[2]; ?>
              </font>
            </td>
            <?php } else { ?>
            <td align="<?php echo (isset($alignLeft) && $alignLeft == true ? 'left' : 'center'); ?>" style="font-family:'Open sans', Arial, sans-serif; text-decoration: none; font-size: 16px; color: #212121; font-weight:<?php echo (isset($textBold) && $textBold == true ? '800' : '300'); ?>; text-align:<?php echo (isset($alignLeft) && $alignLeft == true ? 'left' : 'center'); ?>;">
              <font face="'Open sans', Arial, sans-serif">
                <?php echo $bulletIntro; ?>
              </font>
            </td>
          <?php } ?>
          </tr>
          <tr>
            <td height="40" class="spacer-40-h">&nbsp;</td>
          </tr>
          <?php } ?>
          <?php for ($i = 1; $i <= $number; $i++) { ?>
          <tr class="mobile-stack">
            <td align="center" class="mobile-stack" valign="top" style="display: block !important;">
              <table align="left" border="0" cellpadding="2" cellspacing="0" class="full-width content-row" style="width: 100%; border-bottom-width: 20px; border-bottom-color: #ffffff; border-bottom-style: solid; text-align: left;">
                <tbody>
                  <tr>
                    <td align="right" valign="top" width="20" class="text-normal"
                        <?php if (isset($style) && $style == 'numeric' ) { ?>
                           style="font-family:'Open sans', Arial, sans-serif; text-decoration: none; font-size: 14px; color: #212121; font-weight:800; line-height: 24px;">
                      <font face="'Open sans', Arial, sans-serif">
                        <?php
                          echo $i . '.';
                        } else { ?>
                          style="font-family:'Open sans', Arial, sans-serif; text-decoration: none; font-size: 24px; color: #212121; font-weight:800; line-height: 24px;">
                          <font face="'Open sans', Arial, sans-serif">
                          &bullet;
                        <?php } ?>
                      </font>
                    </td>
                    <td class="<?php echo (isset($noPadding) && $noPadding == true ? '' : 'spacer-10-w'); ?>" width="<?php echo (isset($noPadding) && $noPadding == true ? '0' : '10'); ?>">&nbsp;</td>
                    <td>
						          <table align="left" border="0" cellpadding="2" cellspacing="0">
					              <tbody>
                          <?php if (isset($bulletPointTitle) && $bulletPointTitle != false ) { ?>
      						      	<tr>
      						        	<td class="text-h3" style="font-family:'Open sans', Arial, sans-serif; text-decoration: none; font-size: 16px; color: #212121; font-weight:800;"><font face="'Open sans', Arial, sans-serif"><?php echo ${"title_$i"}; ?></font></td>
      						      	</tr>
								          <?php } ?>
                          <tr>
                            <td class="text-normal" style="font-family:'Open sans', Arial, sans-serif; text-decoration: none; font-size: 14px; color: #212121; font-weight:300;">
                              <font face="'Open sans', Arial, sans-serif">
                              <?php if ( isset(${"link_$i"}) ) {
                                $split = explode("|", ${"text_$i"});
                              ?>
                              <?php echo $split[0]; ?> <a href="<?php echo ${"link_$i"}; echo $json2["{$lang}"]['urchin']; ?>&utm_content=bp<?php echo $i;?>_link" target="_blank" class="text-link" style="white-space: nowrap; text-decoration: none; color: #0e94f7; font-weight: bold;"><?php echo $split[1]; ?></a><?php echo $split[2]; ?>
                              <?php } else { ?>
          						     	  <?php echo ${"text_$i"}; ?>
                              <?php } ?>

                              </font>
                            </td>
                          </tr>
						            </tbody>
						          </table>
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
          <?php
          }
          ?>
        </table>
      </td>
      <td class="<?php echo (isset($noPadding) && $noPadding == true ? '' : 'spacer-40-w'); ?>" width="<?php echo (isset($noPadding) && $noPadding == true ? '0' : '40'); ?>">&nbsp;</td>
   </tr>
    <tr>
      <td height="30" class="spacer-30-h" colspan="5">&nbsp;</td>
    </tr>
  </table>
</div>
<!-- $$$$$ bullet points end -->
