<!-- $$$$$ Sign Off Starts -->
<div>
	<table align="<?php echo (isset($alignLeft) && $alignLeft == true ? 'left' : 'center'); ?>" border="0" cellspacing="0" cellpadding="0" class="m-span10 tbl-610">
		<tr>
			<td height="60" class="spacer-60-h" colspan="5">&nbsp;</td>
		</tr>
		<tr>
			<td class="<?php echo (isset($noPadding) && $noPadding == true ? '' : 'spacer-20-w'); ?>" width="<?php echo (isset($noPadding) && $noPadding == true ? '0' : '20'); ?>">&nbsp;</td>
			<td class="m-hide <?php echo (isset($noPadding) && $noPadding == true ? '' : 'spacer-20-w'); ?>" width="<?php echo (isset($noPadding) && $noPadding == true ? '0' : '20'); ?>">&nbsp;</td>
			<td class="p-mp text-<?php echo (isset($alignLeft) && $alignLeft == true ? 'left' : 'center'); ?>" align="<?php echo (isset($alignLeft) && $alignLeft == true ? 'left' : 'center'); ?>">
				<?php if ( isset($thanks) ) { 
					echo $json1["{$lang}"]['footerTeamText'];
					} else if ( isset($seeyou) ) { 
					echo $json1["{$lang}"]['footerSeeYouText']; 
					} else { 
					echo $json1["{$lang}"]['footerThanksText']; 
				} ?>
			</td>
			<td class="<?php echo (isset($noPadding) && $noPadding == true ? '' : 'spacer-20-w'); ?>" width="<?php echo (isset($noPadding) && $noPadding == true ? '0' : '20'); ?>">&nbsp;</td>
			<td class="m-hide <?php echo (isset($noPadding) && $noPadding == true ? '' : 'spacer-20-w'); ?>" width="<?php echo (isset($noPadding) && $noPadding == true ? '0' : '20'); ?>">&nbsp;</td>
		</tr>
		<tr>
			<td height="20" class="spacer-20-h" colspan="5">&nbsp;</td>
		</tr>
		<tr>
			<td class="<?php echo (isset($noPadding) && $noPadding == true ? '' : 'spacer-20-w'); ?>" width="<?php echo (isset($noPadding) && $noPadding == true ? '0' : '20'); ?>">&nbsp;</td>
			<td class="m-hide <?php echo (isset($noPadding) && $noPadding == true ? '' : 'spacer-20-w'); ?>" width="<?php echo (isset($noPadding) && $noPadding == true ? '0' : '20'); ?>">&nbsp;</td>
			<td class="p-mp text-<?php echo (isset($alignLeft) && $alignLeft == true ? 'left' : 'center'); ?>" align="<?php echo (isset($alignLeft) && $alignLeft == true ? 'left' : 'center'); ?>">
		  		<?php echo $json1["{$lang}"]['footerTeamText'];?> 
			</td>
			<td class="<?php echo (isset($noPadding) && $noPadding == true ? '' : 'spacer-20-w'); ?>" width="<?php echo (isset($noPadding) && $noPadding == true ? '0' : '20'); ?>">&nbsp;</td>
			<td class="m-hide <?php echo (isset($noPadding) && $noPadding == true ? '' : 'spacer-20-w'); ?>" width="<?php echo (isset($noPadding) && $noPadding == true ? '0' : '20'); ?>">&nbsp;</td>
		</tr>
		<tr>
			<td height="40" class="spacer-40-h" colspan="5">&nbsp;</td>
		</tr>
	</table>
</div>
<!-- $$$$$ Sign Off Ends -->