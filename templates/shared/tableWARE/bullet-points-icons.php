<!-- $$$$$ bullet points (icon) start -->
<div>
  <table class="m-span10 tbl-610" bgcolor="#ffffff" align="<?php echo (isset($alignLeft) && $alignLeft == true ? 'left' : 'center'); ?>" width="610" border="0" cellspacing="0" cellpadding="0" style="width: 610px; background-color: #ffffff;">
    <tr>
      <td class="<?php echo (isset($noPadding) && $noPadding == true ? '' : 'spacer-40-w'); ?>" width="<?php echo (isset($noPadding) && $noPadding == true ? '0' : '40'); ?>">&nbsp;</td>
      <td class="m-hide <?php echo (isset($noPadding) && $noPadding == true ? '' : 'spacer-20-w'); ?>" width="<?php echo (isset($noPadding) && $noPadding == true ? '0' : '40'); ?>">&nbsp;</td>
      <td> 

      	<table align="center" border="0" cellpadding="0" cellspacing="0" class="contentWide-width full-width" style="width: 100%;">
          <?php for ($i = 1; $i <= $number; $i++) { ?>
          <tr class="mobile-stack">
            <td align="center" class="mobile-stack" valign="top" style="display: block !important;">
              <table align="left" border="0" cellpadding="2" cellspacing="0" class="full-width content-row" style="width: 100%; border-bottom-width: 20px; border-bottom-color: #ffffff; border-bottom-style: solid; text-align: left;">
                <tbody>
                  <tr>
                    <td align="left" valign="top" width="30" class="inlineIcon" style="padding-top: 5px;">
                      <img class="retinaReady" alt="" src="<?php echo ${"icon_$i"}; ?>" width="<?php echo ${"width_$i"}; ?>" style="max-width: <?php echo ${"width_$i"}; ?>px; display: block; width: 100%; font-family: sans-serif; border: 0px; color: #ffffff; font-size: 11px;" border="0">
                    </td>
                    <td class="<?php echo (isset($noPadding) && $noPadding == true ? '' : 'spacer-10-w'); ?>" width="<?php echo (isset($noPadding) && $noPadding == true ? '0' : '10'); ?>">&nbsp;</td>
                    <td>
						          <table align="left" border="0" cellpadding="2" cellspacing="0">
					              <tbody>
                          <?php if (isset($bulletTitle) && $bulletTitle != false ) { ?>
      						      	<tr>
      						        	<td class="text-h3" style="font-family:'Open sans', Arial, sans-serif; text-decoration: none; font-size: 16px; color: #212121; font-weight:800;"><font face="'Open sans', Arial, sans-serif"><?php echo ${"title_$i"}; ?></font></td>
      						      	</tr>
								          <?php } ?>
      						     	  <tr>
                            <td class="text-normal" style="font-family:'Open sans', Arial, sans-serif; text-decoration: none; font-size: 14px; color: #212121; font-weight:300;"><font face="'Open sans', Arial, sans-serif"><?php echo ${"text_$i"}; ?></font></td>
      						      	</tr>
						            </tbody>
						          </table>
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
          <?php
          }
          ?>
        </table>
      </td>
      <td class="<?php echo (isset($noPadding) && $noPadding == true ? '' : 'spacer-40-w'); ?>" width="<?php echo (isset($noPadding) && $noPadding == true ? '0' : '40'); ?>">&nbsp;</td>
      <td class="m-hide <?php echo (isset($noPadding) && $noPadding == true ? '' : 'spacer-20-w'); ?>" width="<?php echo (isset($noPadding) && $noPadding == true ? '0' : '20'); ?>">&nbsp;</td>
    </tr>
    <tr>
      <td height="30" class="spacer-30-h" colspan="5">&nbsp;</td>
    </tr>
  </table>
</div>
<!-- $$$$$ bullet points (icon) end -->