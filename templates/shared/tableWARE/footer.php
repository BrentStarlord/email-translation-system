		<!-- $$$$$ Footer starts-->
		<table class="mp-footer m-span10 tbl-610" align="center" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td class="<?php echo (isset($noPadding) && $noPadding == true ? '' : 'spacer-20-w'); ?>" width="<?php echo (isset($noPadding) && $noPadding == true ? '0' : '20'); ?>">&nbsp;</td>
				<td class="m-hide <?php echo (isset($noPadding) && $noPadding == true ? '' : 'spacer-20-w'); ?>" width="<?php echo (isset($noPadding) && $noPadding == true ? '0' : '20'); ?>">&nbsp;</td>
			    <td>
			        <table class="m-span10 tbl-100" cellspacing="0" cellpadding="0" align="center">
			            <tbody>
			                <tr>
			                  <td height="20" class="spacer-20-h" colspan="1">&nbsp;</td>
			                </tr>
			                <tr>
			                    <td class="mp-footer-break">
			                        <p class="mp-footer-paragraph">
			                            <?php echo $json1["{$lang}"]['footerText'];?>
			                        </p>
			                    </td>
			                </tr>
			                <tr>
			                  <td height="20" class="spacer-20-h" colspan="1">&nbsp;</td>
			                </tr>
			            </tbody>
			        </table>
			    </td>
				<td class="<?php echo (isset($noPadding) && $noPadding == true ? '' : 'spacer-20-w'); ?>" width="<?php echo (isset($noPadding) && $noPadding == true ? '0' : '20'); ?>">&nbsp;</td>
				<td class="m-hide <?php echo (isset($noPadding) && $noPadding == true ? '' : 'spacer-20-w'); ?>" width="<?php echo (isset($noPadding) && $noPadding == true ? '0' : '20'); ?>">&nbsp;</td>
			</tr>
		</table>
		<!-- $$$$$ Footer ends-->
		<table class="m-span10 tbl-100" bgcolor="#ccc" cellspacing="0" cellpadding="0" align="center" style="width: 100%">
			<tbody>
				<tr>
					<td height="20" class="spacer-20-h" colspan="1" style="line-height: 20px; font-size: 20px; height: 20px">&nbsp;</td>
				</tr>
				<tr>
					<td class="m-padding p-mp text-center text-small" align="center" style="text-align: center; font-family: Verdana, Arial, sans-serif; font-size: 11px"><a href="<%asm_group_unsubscribe_raw_url%>"><span class="text-link" style="text-decoration: underline; color: #000000">Unsubscribe</span></a></td>
				</tr>
				<tr>
					<td height="20" class="spacer-20-h" colspan="1" style="line-height: 20px; font-size: 20px; height: 20px">&nbsp;</td>
				</tr>
			</tbody>
		</table>
	</body>
</html>