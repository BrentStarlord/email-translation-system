<!-- $$$$$ text starts -->
<div>
  <table class="m-span9 h2-block tbl-610" bgcolor="#ffffff" align="<?php echo (isset($alignLeft) && $alignLeft == true ? 'left' : 'center'); ?>" width="550" border="0" cellspacing="0" cellpadding="0" style="background-color: #ffffff;">
    <tbody>
      <tr>
        <td height="10" style="height: 10px; line-height: 10px; font-size: 10px; mso-line-height-rule: exactly;" colspan="3">&nbsp;</td>
      </tr>
      <tr>
        <td class="m-hide <?php echo (isset($noPadding) && $noPadding == true ? '' : 'spacer-40-w'); ?>" width="<?php echo (isset($noPadding) && $noPadding == true ? '0' : '40'); ?>">&nbsp;</td>
        <td class="h2 make-<?php echo (isset($h2Color)) ? $h2Color : 'standard' ; ?>" align="<?php echo (isset($alignLeft) && $alignLeft == true ? 'left' : 'center'); ?>" style="font-family:'Open sans', Arial, sans-serif; text-decoration: none; font-size: 20px; padding: 20px; font-weight:800; text-align:<?php echo (isset($alignLeft) && $alignLeft == true ? 'left' : 'center'); ?>;"><font face="'Open sans', Arial, sans-serif">
          <?php echo $paragraphText; ?> 
        </font></td>
        <td class="m-hide <?php echo (isset($noPadding) && $noPadding == true ? '' : 'spacer-40-w'); ?>" width="<?php echo (isset($noPadding) && $noPadding == true ? '0' : '40'); ?>">&nbsp;</td>
      </tr>
      <tr>
        <td height="40" style="height: 40px; line-height: 40px; font-size: 40px; mso-line-height-rule: exactly;" colspan="3">&nbsp;</td>
      </tr>
    </tbody>
  </table>
</div>
<!-- $$$$$ text ends -->