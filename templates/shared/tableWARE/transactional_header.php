<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
  <head>
    <title><?php echo $json2["{$lang}"]['previewText'];?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="x-apple-disable-message-reformatting">
    <meta name="author" content="Brent Starling" />
    <!--[if gte mso 9]><!-->
    
    <!--<![endif]-->
    <link rel="stylesheet" href="../../css/inline.css">
	<link rel="stylesheet" href="../../css/embed.css" data-embed>
    <!--[if gte mso 9]>
    <xml>
    <o:OfficeDocumentSettings>
    <o:AllowPNG/>
    <o:PixelsPerInch>96</o:PixelsPerInch>
    </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->
  </head>
  <body class="body" style="width: 100%; margin: 0; padding: 0;">
    <table align="center" width="100%" border="0" cellspacing="0" cellpadding="0" class="tbl-100">
      <tbody>
        <tr>
          <td align="center">
            <!-- ////// HEADER ENDS -->
            <div class="previewText">
              <?php echo $json2["{$lang}"]['previewText'];?>
      			</div>
            <div class="weblinkText" style="font-size: 12px; color: #212121">
              <table align="center" border="0" cellspacing="0" cellpadding="0" class="m-span10 tbl-610" style="width: 610px">
                <tr>
                  <td height="20" class="spacer-20-h" style="line-height: 20px; font-size: 20px; height: 20px">&nbsp;</td>
                </tr>
                <tr>
                  <td class="m-padding p-mp text-center text-small" align="center" style="text-align: center; font-family: Verdana, Arial, sans-serif; font-size: 11px">
                    <?php echo $json1["{$lang}"]['viewInBrowserText1'];?> <a href="[Weblink]"><span class="text-link" style="text-decoration: underline; color: #000000"><?php echo $json1["{$lang}"]['viewInBrowserText2'];?></span></a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;SUPPLIER ID : [%supplier_id | Not Set%]
                  </td>
                </tr>
                <tr>
                  <td height="20" class="spacer-20-h" style="line-height: 20px; font-size: 20px; height: 20px">&nbsp;</td>
                </tr>
              </table>
            </div>
            <!-- ////// CONTENT STARTS -->