<!-- $$$$$ CTA starts -->
<div>
  <table class="m-span10 tbl-610" bgcolor="#ffffff" align="<?php echo (isset($alignLeft) && $alignLeft == true ? 'left' : 'center'); ?>" width="610" border="0" cellspacing="0" cellpadding="0" style="width: 610px; background-color: #ffffff;">
    <tr>
      <td class="<?php echo (isset($noPadding) && $noPadding == true ? '' : 'spacer-20-w'); ?>" width="<?php echo (isset($noPadding) && $noPadding == true ? '0' : '20'); ?>">&nbsp;</td>
      <td class="m-hide <?php echo (isset($noPadding) && $noPadding == true ? '' : 'spacer-20-w'); ?>" width="<?php echo (isset($noPadding) && $noPadding == true ? '0' : '20'); ?>">&nbsp;</td>
      <td align="<?php echo (isset($alignLeft) && $alignLeft == true ? 'left' : 'center'); ?>" style="font-family:'Open sans', Arial, sans-serif; text-decoration: none; font-size: 16px; color: #212121; font-weight:<?php echo (isset($textBold) && $textBold == true ? '800' : '300'); ?>; text-align:<?php echo (isset($alignLeft) && $alignLeft == true ? 'left' : 'center'); ?>;">
			<table class="<?php echo (isset($ctaColor) ) ? 'make-' . $ctaColor : '' ; ?> " border="0" cellspacing="0" cellpadding="0" align="<?php echo (isset($alignLeft) && $alignLeft == true ? 'left' : 'center'); ?>">
		        <tbody>
	              <tr>
	                <td align="<?php echo (isset($alignLeft) && $alignLeft == true ? 'left' : 'center'); ?>">
	                  <table border="0" cellspacing="0" cellpadding="0" width="100%" class="btn">
	                    <tr>
	                    	<td class="<?php echo (isset($noPadding) && $noPadding == true ? '' : 'spacer-50-w'); ?>" width="<?php echo (isset($noPadding) && $noPadding == true ? '0' : '50'); ?>">&nbsp;</td>
							<td align="<?php echo (isset($alignLeft) && $alignLeft == true ? 'left' : 'center'); ?>" class="btn-cell">
		                    	<?php if (isset($mailto) && $mailto = true) {?>
		                    		<a href="<?php echo $btnLink; ?>" title="<?php echo $btnTitle; ?>" target="_blank" class="btn-link"><?php echo $btnText; ?></a>
		                    	<?php } else { ?>
									<a href="<?php echo $btnLink; ?><?php echo ( isset($noUrchin) && $noUrchin == true ) ? '' : $json2["{$lang}"]['urchin'] . '&utm_content=link'?>" title="<?php echo $btnTitle; ?>" target="_blank" class="btn-link"><?php echo $btnText; ?></a>
		                    	<?php } ?>
	                      	</td>
	                    	<td class="<?php echo (isset($noPadding) && $noPadding == true ? '' : 'spacer-5-w'); ?>" width="<?php echo (isset($noPadding) && $noPadding == true ? '0' : '5'); ?>">&nbsp;</td>
	                    	<td class="btn-icon"><img class="retinaReady" alt="" src="https://s3-eu-west-1.amazonaws.com/rcmp-marketing-prod/images/email/cta-arrow-transparent.png" width="15" style="max-width: 15px; display: block; width: 100%; font-family: sans-serif; border: 0px; color: #ffffff; font-size: 11px;" border="0"></td>
	                    	<td class="<?php echo (isset($noPadding) && $noPadding == true ? '' : 'spacer-50-w'); ?>" width="<?php echo (isset($noPadding) && $noPadding == true ? '0' : '50'); ?>">&nbsp;</td>
	                    </tr>
	                  </table>
	                </td>
	              </tr>
	            </tbody>
	          </table>
			</td>
      <td class="<?php echo (isset($noPadding) && $noPadding == true ? '' : 'spacer-20-w'); ?>" width="<?php echo (isset($noPadding) && $noPadding == true ? '0' : '20'); ?>">&nbsp;</td>
      <td class="m-hide <?php echo (isset($noPadding) && $noPadding == true ? '' : 'spacer-20-w'); ?>" width="<?php echo (isset($noPadding) && $noPadding == true ? '0' : '20'); ?>">&nbsp;</td>
    </tr>
    <tr>
      <td height="30" class="spacer-30-h" colspan="5">&nbsp;</td>
    </tr>
  </table>
</div>

<!-- $$$$$ CTA ends -->