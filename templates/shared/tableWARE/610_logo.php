<!-- $$$$$ Logo module starts -->
<div>
    <table width="610" class="tbl-610 m-span10 mp-header" align="<?php echo (isset($alignCenter) && $alignCenter == true) ? 'center' : 'left' ?>" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td height="10" class="spacer-20-h" colspan="3">&nbsp;</td>
        </tr>
        <tr align="<?php echo (isset($alignCenter) && $alignCenter == true) ? 'center' : 'left' ?>">
            <td width="20" class="spacer-20-w" colspan="1">&nbsp;</td>
            <td align="<?php echo (isset($alignCenter) && $alignCenter == true) ? 'center' : 'left' ?>">
                <a title="<?php echo $json1["{$lang}"]['headLinkTitle'];?>" target="_blank" href="https://marketplace.rentalcars.com/#/login<?php echo $json2["{$lang}"]['urchin'];?>&utm_content=header" > <img class="retinaReady" alt="Marketplace by Rentalcars.com" src="https://marketing-image-production.s3.amazonaws.com/uploads/e50393ab2b1b62bbf2f786ba2b270a50c6daeb5c7d550be9f101974258319cf6f6e60210ae5adef3f848401608b5f099ffe9b554f6bd6a11f57644607554102d.png" width="180" style="display: block; color: #fff; border: 0;" border="0"></a>
            </td>
            <td width="20" class="spacer-20-w" colspan="1">&nbsp;</td>
        </tr>
        <tr>
            <td height="10" class="spacer-20-h" colspan="3">&nbsp;</td>
        </tr>
    </table>
</div>
<!-- $$$$$ Logo module ends -->