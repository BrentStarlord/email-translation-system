<!-- $$$$$ text starts -->
<div>
  <table class="m-span10 tbl-610" bgcolor="#ffffff" align="<?php echo (isset($alignLeft) && $alignLeft == true ? 'left' : 'center'); ?>" width="610" border="0" cellspacing="0" cellpadding="0" style="width: 610px; background-color: #ffffff;">
    <tr>
      <td class="<?php echo (isset($noPadding) && $noPadding == true ? '' : 'spacer-20-w'); ?>" width="<?php echo (isset($noPadding) && $noPadding == true ? '0' : '20'); ?>">&nbsp;</td>
      <td class="m-hide <?php echo (isset($noPadding) && $noPadding == true ? '' : 'spacer-20-w'); ?>" width="<?php echo (isset($noPadding) && $noPadding == true ? '0' : '20'); ?>">&nbsp;</td>
      <td align="<?php echo (isset($alignLeft) && $alignLeft == true ? 'left' : 'center'); ?>" style="font-family:'Open sans', Arial, sans-serif; text-decoration: none; font-size: 16px; color: #212121; font-weight:<?php echo (isset($textBold) && $textBold == true ? '800' : '300'); ?>; text-align:<?php echo (isset($alignLeft) && $alignLeft == true ? 'left' : 'center'); ?>;"><font face="'Open sans', Arial, sans-serif">
                    <?php echo $paragraphText; ?>
                  </font></td>
      <td class="<?php echo (isset($noPadding) && $noPadding == true ? '' : 'spacer-20-w'); ?>" width="<?php echo (isset($noPadding) && $noPadding == true ? '0' : '20'); ?>">&nbsp;</td>
      <td class="m-hide <?php echo (isset($noPadding) && $noPadding == true ? '' : 'spacer-20-w'); ?>" width="<?php echo (isset($noPadding) && $noPadding == true ? '0' : '20'); ?>">&nbsp;</td>
    </tr>
    <?php if(!isset($noBottomPadding) || $noBottomPadding = false) { ?>
    <tr>
      <td height="30" class="spacer-30-h" colspan="5">&nbsp;</td>
    </tr>
    <?php } ?>
  </table>
</div>

<!-- $$$$$ text ends -->
