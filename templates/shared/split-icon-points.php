<?php if ( isset($introText) && $introText != false ) { ?>
<table border="0" cellpadding="0" cellspacing="0" class="content-width" style="width: 440px;">
  <tbody>
    <tr>
      <td align="left" valign="top" class="text-h4 spacerTop content-row" style="font-family: Verdana, Arial, sans-serif; font-size: 16px; line-height: 24px; border-top-width: 10px; border-top-color: #ffffff; border-top-style: solid; border-bottom-width: 20px; border-bottom-color: #ffffff; border-bottom-style: solid;">
        <?php echo $introText; ?>
      </td>
    </tr>
  </tbody>
</table>
<?php
}
?>
<table border="0" cellpadding="0" cellspacing="0" class="content-width" style="width: 440px;">
  <tbody>
    <?php
    for ($i = 1; $i <= $number; $i++) { ?>
    <tr>
      <td align="left" class="mobile-stack" valign="top">
        <table align="left" border="0" cellpadding="2" cellspacing="0" class="content-two-col content-row" style="width: 220px; border-bottom-width: 20px; border-bottom-color: #ffffff; border-bottom-style: solid; text-align: left;">
          <tbody>
            <tr>
              <td align="left" valign="top" width="40" class="inlineIcon" style="padding-top: 5px;">
                <img class="retinaReady" alt="" src="<?php echo ${"icon_a_$i"}; ?>" width="16" style="max-width: 16px; display: block; width: 100%; font-family: sans-serif; border: 0px; color: #ffffff; font-size: 11px;" border="0">
              </td>
              <td>
                <table width="100%" align="left" border="0" cellpadding="2" cellspacing="0">
                  <tbody>
                    <tr>
                      <td class="text-h3" style="font-family: Verdana, Arial, sans-serif; font-size: 16px; font-weight: bold;">
                        <?php echo ${"text_a_$i"}; ?>
                      </td>
                    </tr>
                    <tr>
                      <td class="text-normal" style="font-family: Verdana, Arial, sans-serif; font-size: 14px;">
                        <?php echo ${"variable_a_$i"}; ?>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
      <td align="left" class="mobile-stack" valign="top">
        <table align="left" border="0" cellpadding="2" cellspacing="0" class="content-two-col content-row" style="width: 220px; border-bottom-width: 20px; border-bottom-color: #ffffff; border-bottom-style: solid; text-align: left;">
          <tbody>
            <tr>
              <td align="left" valign="top" width="40" class="inlineIcon" style="padding-top: 5px;">
                <img class="retinaReady" alt="" src="<?php echo ${"icon_b_$i"}; ?>" width="16" style="max-width: 16px; display: block; width: 100%; font-family: sans-serif; border: 0px; color: #ffffff; font-size: 11px;" border="0">
              </td>
              <td>
                <table width="100%" align="left" border="0" cellpadding="2" cellspacing="0">
                  <tbody>
                    <tr>
                      <td class="text-h3" style="font-family: Verdana, Arial, sans-serif; font-size: 16px; font-weight: bold;">
                        <?php echo ${"text_b_$i"}; ?>
                      </td>
                    </tr>
                    <tr>
                      <td class="text-normal" style="font-family: Verdana, Arial, sans-serif; font-size: 14px;">
                        <?php echo ${"variable_b_$i"}; ?>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
    <?php
    }
    ?>
  </tbody>
</table>