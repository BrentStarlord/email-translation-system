<?php
$split = explode("|", $paragraphText);
?>

<table align="<?php echo (isset($alignLeft) && $alignLeft == true ? 'left' : 'center'); ?>" border="0" cellpadding="<?php echo (isset($noPadding) && $noPadding == true ? '0' : '20'); ?>" cellspacing="0">
  <tr>
    <td align="<?php echo (isset($alignLeft) && $alignLeft == 'left' ? 'left' : 'center'); ?>" class="text-h4 content-center" style="font-family: Verdana, Arial, sans-serif; font-size: 16px; line-height: 24px; border-left-width: 10px; border-left-color: #ffffff; border-left-style: solid; border-bottom-width: 20px; border-bottom-color: #ffffff; border-bottom-style: solid;">
      <?php echo $split[0]; ?> <a href="<?php echo ( isset($linkURL) ) ? $linkURL : '#'; ?>" target="_blank" class="text-link" style="white-space: nowrap; text-decoration: none; color: #0e94f7; font-weight: bold;"><?php echo $split[1]; ?></a> <?php echo $split[2]; ?>
    </td>
  </tr>
</table>