<table border="0" cellpadding="<?php echo (isset($noPadding) && $noPadding == true ? '0' : '20'); ?>" cellspacing="0" class="content-width content-row content-center" style="width: 440px; border-bottom-width: <?php echo (isset($noPadding) && $noPadding == true ? '0' : '20'); ?>px; border-bottom-color: #ffffff; border-bottom-style: solid; text-align: center;">
  <tbody>
    <tr>
      <td align="center" valign="top">
        <table cellspacing="0" cellpadding="0" border="0" align="center" class="button-cta" style="width: 85%; height: 25px; line-height: 18px; font-size: 16px; border-radius: 5px; text-align: center;" bgcolor="#0e94f7">
          <tbody>
            <tr>
              <td align="center">
                <table cellpadding="0" cellspacing="0" border="0" align="center" class="full-width mobile-auto" style="width: 100%;">
                  <tbody>
                    <tr>
                      <td valign="top" align="right" width="<?php echo $buttonWidth; ?>%" class="text-cta button-text" style="font-family: Verdana, Arial, sans-serif; font-size: 16px; text-decoration: none; color: #ffffff;">
                        <a title="<?php echo $buttonText; ?>" target="_blank" href="<?php echo $buttonLink; ?>" class="text-cta" style="font-family: Verdana, Arial, sans-serif; font-size: 16px; text-decoration: none; color: #ffffff; display: block;">
                          <span style="display: block; border-radius: 5px; border-color: #0e94f7; border-style: solid; border-width: 11px 4px;">
                            <?php echo $buttonText; ?>
                          </span>
                        </a>
                      </td>
                      <td valign="middle" align="left">
                        <a title="<?php echo $buttonText; ?>" target="_blank" href="<?php echo $buttonLink; ?>" class="text-cta" style="font-family: Verdana, Arial, sans-serif; font-size: 16px; text-decoration: none; color: #ffffff; display: block;">
                          <span style="display: block; border-radius: 5px; border-color: #0e94f7; border-style: solid; border-width: 11px 4px;">
                            <img class="retinaReady" alt="" src="https://marketing-image-production.s3.amazonaws.com/uploads/6324d44f422d1a23efa55d3a1df693d07697fb9b5a4e0c43cfaed392bacbb44516d3f1fae634e7cda39d862dd9315062d086871b244b14976e9853f50c045f52.png" width="15" style="max-width: 15px; display: block; width: 100%; font-family: sans-serif; border: 0px; color: #ffffff; font-size: 11px;" border="0">
                          </span>
                        </a>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
  </tbody>
</table>