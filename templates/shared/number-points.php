<table align="<?php echo (isset($alignLeft) && $alignLeft == true ? 'left' : 'center'); ?>" border="0" cellpadding="<?php echo (isset($noPadding) && $noPadding == true ? '0' : '20'); ?>" cellspacing="0">
  <tr>
    <td align="<?php echo (isset($alignLeft) && $alignLeft == 'left' ? 'left' : 'center'); ?>">
     <table align="<?php echo (isset($alignLeft) && $alignLeft == true ? 'left' : 'center'); ?>" border="0" cellpadding="10" cellspacing="0">
        <tr>
          <td align="<?php echo (isset($alignLeft) && $alignLeft == 'left' ? 'left' : 'center'); ?>"valign="top" class="text-h2 content-row content-center" style="font-family: Verdana, Arial, sans-serif; font-size: 24px; border-bottom-width: 10px; border-bottom-color: #ffffff; border-bottom-style: solid;">
            <?php echo $titleText; ?>
          </td>
        </tr>
        <tr>
          <td align="center" class="text-h4 content-center" style="font-family: Verdana, Arial, sans-serif; font-size: 16px; line-height: 24px;"><?php echo $paragraphText; ?></td>
        </tr>
        <tr class="mobile-stack">
          <td valign="middle">
            <table align="left" border="0" cellpadding="10" cellspacing="0">
              <?php for ($i = 1; $i <= $number; $i++) { ?>
              <tr>
                <td>
                  <table align="left" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                      <td class="text-featurePoint featureBullet featureBullet-blue" style="font-family: Verdana, Arial, sans-serif; font-size: 18px; color: #ffffff; border-radius: 50%; padding: 7px 12px;" align="center" bgcolor="#0e94f7"><?php echo $i;?></td>
                    </tr>
                  </table>
                </td>
                <td align="left" valign="middle" class="text-h4" style="font-family: Verdana, Arial, sans-serif; font-size: 16px; line-height: 24px;">
                  <?php echo ${"text$i"}; ?> 
                </td>
              </tr>
              <?php
              }
              ?>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>