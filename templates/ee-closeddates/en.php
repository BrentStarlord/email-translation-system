<?php

global $lang;

$lang = basename($_SERVER['PHP_SELF'],'.php');

$data1 = file_get_contents ("../shared/shared.json", FILE_USE_INCLUDE_PATH);
$json1 = json_decode($data1,true);

$data2 = file_get_contents ("translations.json", FILE_USE_INCLUDE_PATH);
$json2 = json_decode($data2,true);

include ('../shared/tableWARE/transactional_header.php');

include ('../shared/tableWARE/container_start.php');

	include ('../shared/tableWARE/610_logo.php');
	
	$alignLeft = false;
	$titleText = $json2["{$lang}"]['titleText'];
	$subtitle = true;
	$subtitleText = $json2["{$lang}"]['subtitleText'];
	include ('../shared/tableWARE/hero_text.php');


	include ('../shared/tableWARE/spacer_row_20.php');

	$alignLeft = true;
	$textBold = false;
	$paragraphText = $json2["{$lang}"]['introductionText1'];
	include ('../shared/tableWARE/text_block.php');

	$textBold = false;
	$paragraphText = $json2["{$lang}"]['introductionText2'];
	include ('../shared/tableWARE/text_block.php');

	$alignLeft = false;
	$textBold = false;
	$btnText = $json2["{$lang}"]['buttonText'];
	$mailto = true;
	$btnLink = 'mailto:help@marketplace.rentalcars.com?subject=Our%20Holiday%20Dates';
	$btnTitle = $json2["{$lang}"]['buttonTitle'];
	include ('../shared/tableWARE/cta.php');

	include ('../shared/tableWARE/spacer_row_40.php');

	$textBold = false;
	$alignLeft = true;
	$paragraphText = $json2["{$lang}"]['introductionText3'];
	include ('../shared/tableWARE/text_block.php');


	$textBold = true;
	$alignLeft = true;
	$paragraphText = $json2["{$lang}"]['introductionText4'];
	include ('../shared/tableWARE/text_block.php');

	$textBold = false;
	$alignLeft = true;
	$paragraphText = $json2["{$lang}"]['introductionText5'];
	include ('../shared/tableWARE/text_block.php');

	$textBold = false;
	$alignLeft = true;
	$paragraphText = $json2["{$lang}"]['introductionText6'];
	include ('../shared/tableWARE/text_block.php');

	$textBold = false;
	$alignLeft = false;
	$paragraphText = $json2["{$lang}"]['introductionText7'];
	include ('../shared/tableWARE/h2_block.php');

	$alignLeft = true;
	include ('../shared/tableWARE/sign_off.php');

	include ('../shared/tableWARE/spacer_row_20.php');


	include ('../shared/tableWARE/footer.php');

include ('../shared/tableWARE/container_end.php');

?>