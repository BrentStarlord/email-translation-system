<?php

global $lang;

$lang = basename($_SERVER['PHP_SELF'],'.php');

$data1 = file_get_contents ("../shared/shared.json", FILE_USE_INCLUDE_PATH);
$json1 = json_decode($data1,true);

$data2 = file_get_contents ("translations.json", FILE_USE_INCLUDE_PATH);
$json2 = json_decode($data2,true);
//print_r($json2);

include ('../shared/page-start.php');

?>

      <table align="center" border="0" cellpadding="0" cellspacing="0" class="wrapper-body" width="100%" style="" bgcolor="#e6e6e6">
         <tr>
            <td align="center">
               <?php
               include ('../shared/logo-strip.php');
               ?>
               <table align="center" border="0" cellpadding="0" cellspacing="0" class="wrapper-width wrapper-content" style="width: 640px;" bgcolor="#ffffff">
                  <tr>
                     <td>
                        <table border="0" cellpadding="0" cellspacing="0" class="full-width" align="center" style="width: 100%;">
                           <tr>
                              <td align="center" valign="top">
                                 <table border="0" cellpadding="0" cellspacing="0" class="contentWide-width full-width" style="width: 100%; padding: 0 20px;">
                                    <tr>
                                       <td align="center" class="mobile-stack" valign="top">
                                          <?php
                                          $icon = true;
                                          $icon_type = 'res-created';
                                          $titleText = $json2["{$lang}"]['titleText'];
                                          $noPadding = true;
                                          include ('../shared/h1-title.php');
                                          $titleText = $json2["{$lang}"]['subtitleText'];
                                          include ('../shared/h2-title.php');
                                          ?>
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                           <tr>
                               <td align="center" valign="top">
                                 <table border="0" cellpadding="0" cellspacing="0" class="contentWide-width full-width" style="width: 100%; padding: 0 20px;">
                                    <tr>
                                       <td align="center" class="mobile-stack" valign="top">
                                          <?php
                                          $noPadding = false;
                                          $buttonLink = '{{baseUrl}}/reservation-management';
                                          $buttonWidth = 73;
                                          $buttonText = $json2["{$lang}"]['buttonText'];
                                          include ('../shared/button-table.php');
                                          ?>
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                           <tr>
                              <td align="center" valign="top">
                                 <table border="0" cellpadding="0" cellspacing="0" class="content-width" style="width: 440px;">
                                    <tr>
                                       <td align="left" class="mobile-stack" valign="top">
                                          <?php
                                          $alignLeft = true;
                                          $noPadding = true;
                                          $titleText = $json2["{$lang}"]['rdTitle'];
                                          $paragraphText1 = $json2["{$lang}"]['rdMPRef'] . " {{supplierReservationReference}}";
                                          $paragraphText2 = $json2["{$lang}"]['rdRCRef'] . " {{rentalcarsReservationReference}}";
                                          include ('../shared/h2-title-with-paragraph.php');
                                          ?>
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                           <tr>
                              <td align="center" valign="top">
                                 <table border="0" cellpadding="20" cellspacing="0" class="contentWide-width full-width hr-bottom" style="width: 100%; border-bottom-width: 1px; border-bottom-color: #5d5d5d; border-bottom-style: solid; padding: 0 20px;">
                                    <tr>
                                       <td align="center" class="mobile-stack" valign="top">
                                          <table border="0" cellpadding="0" cellspacing="0" class="content-width" style="width: 440px;">
                                             <tr>
                                                <td align="left" class="mobile-stack" valign="top">
                                                   <?php
                                                   
                                                   $number = 2;
                                                   $introText = false;
                                                   $icon_a_1 = 'https://marketing-image-production.s3.amazonaws.com/uploads/136e65ff950301b6ee63c218368633ca9e286964c87af467fe5aa46995ffed831d34b8cdd688ac422842e49ca2dec83a0f690d1a6f4d6dd7858f8446f7695f5d.png';
                                                   $text_a_1 = $json2["{$lang}"]['rdPickDate'];
                                                   $variable_a_1 = '{{pickUpDateTime}}';
                                                   $icon_b_1 = 'https://marketing-image-production.s3.amazonaws.com/uploads/e296f76297c3702edd64418c1770815301e8b23fccc19519da9ff4a8f8db336dba938aa5820b18b79d0cc8a56934c9d7d96517f829d52dd840927ea1c041e5c6.png';
                                                   $text_b_1 = $json2["{$lang}"]['rdPickLoc'];
                                                   $variable_b_1 = '{{pickUpLocationName}}';
                                                   $icon_a_2 = 'https://marketing-image-production.s3.amazonaws.com/uploads/136e65ff950301b6ee63c218368633ca9e286964c87af467fe5aa46995ffed831d34b8cdd688ac422842e49ca2dec83a0f690d1a6f4d6dd7858f8446f7695f5d.png';
                                                   $text_a_2 = $json2["{$lang}"]['rdDropDate'];
                                                   $variable_a_2 = '{{dropOffDateTime}}';
                                                   $icon_b_2 = 'https://marketing-image-production.s3.amazonaws.com/uploads/e296f76297c3702edd64418c1770815301e8b23fccc19519da9ff4a8f8db336dba938aa5820b18b79d0cc8a56934c9d7d96517f829d52dd840927ea1c041e5c6.png';
                                                   $text_b_2 = $json2["{$lang}"]['rdDropLoc'];
                                                   $variable_b_2 = '{{dropOffLocationName}}';
                                                   include ('../shared/split-icon-points.php');

                                                   $number = 4;
                                                   $titleText = false;
                                                   $introText = false;
                                                   $alignLeft = true;

                                                   $icon_1 = 'https://marketing-image-production.s3.amazonaws.com/uploads/7d0e221ce0a06e68c971644e8253e1d7eb4565ea84426029ca4d8f1b12d1d261552896d792496d88cf19b2ab1153887660a5d1aaa506222be406a034609cbf89.png';
                                                   $width_1 = 20;
                                                   $title_1 = $json2["{$lang}"]['rdCarClass'];
                                                   $text_1 = "{{carClass}}";

                                                   $icon_2 = 'https://marketing-image-production.s3.amazonaws.com/uploads/7d0e221ce0a06e68c971644e8253e1d7eb4565ea84426029ca4d8f1b12d1d261552896d792496d88cf19b2ab1153887660a5d1aaa506222be406a034609cbf89.png';
                                                   $width_2 = 20;
                                                   $title_2 = $json2["{$lang}"]['rdMakeModel'];
                                                   $text_2 = "{{makesAndModels}}";

                                                   $icon_3 = 'https://marketing-image-production.s3.amazonaws.com/uploads/2ff908a4cd8820bd776ecab4bf3fe0ea83d1d19fbcb3ad1b92d93ae7e2b4ebbeac7d27ef93c9e8140cb5569970b699c39cd71135f07dbe01f0944c748a8dde88.png';
                                                   $width_3 = 17;
                                                   $title_3 = $json2["{$lang}"]['rdExtras'];
                                                   $text_3 = "{{bookedExtras}}";

                                                   $icon_4 = 'https://marketing-image-production.s3.amazonaws.com/uploads/12070b830f1baa7b49e394a6b3648a0460d4c796ebdcca928542135dfbf1a9344780a56206174143d4bbc5ad7fbb6f7a5f904e1c2bfe0b3c55800f9ab448fb8b.png';
                                                   $width_4 = 18;
                                                   $title_4 = $json2["{$lang}"]['rdTotalPaid'];
                                                   $text_4 = "<tr><td class='text-normal' style='font-family: Verdana, Arial, sans-serif; font-size: 14px;'>". $json2["{$lang}"]['rdRentalTotal'].": {{salePrice}}</td></tr>"
                                                       ."<tr><td class='text-normal' style='font-family: Verdana, Arial, sans-serif; font-size: 14px;'>".$json2["{$lang}"]['rdExtrasTotal']."</td></tr>"
                                                       ."{{#extrasAmounts}}"
                                                       ."<tr><td class='text-normal' style='font-family: Verdana, Arial, sans-serif; font-size: 14px;padding-left:30px;'>{{.}}</td></tr>"
                                                       ."{{/extrasAmounts}}"
                                                       ."<tr><td class='text-normal' style='font-family: Verdana, Arial, sans-serif; font-size: 14px; font-weight: bold;'>".$json2["{$lang}"]['rdTotal']." {{currencyIso}} {{totalAmounts}}</td></tr>";
                                                   
                                                   include ('../shared/icon-points.php');

                                                   ?>
                                                </td>
                                             </tr>
                                          </table>
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>

                           <tr>
                              <td align="center" valign="top">
                                 <table border="0" cellpadding="20" cellspacing="0" class="contentWide-width full-width" style="width: 100%; border-bottom-width: 1px; border-bottom-color: #ffffff; border-bottom-style: solid; padding: 0 20px;">
                                    <tr>
                                       <td align="center" class="mobile-stack" valign="top">
                                          <table border="0" cellpadding="0" cellspacing="0" class="content-width" style="width: 440px;">
                                             <tr>
                                                <td align="left" class="mobile-stack" valign="top">
                                                   
                                                   <?php
                                                   $alignLeft = false;
                                                   $noPadding = false;
                                                   $titleText = $json2["{$lang}"]['cdTitle'];
                                                   include ('../shared/h2-title.php');

                                                   ?>
                                                </td>
                                             </tr>
                                             <tr>
                                                <td>
                                                   <?php
                                                   $number = 10;
                                                   $titleText = false;
                                                   $introText = false;
                                                   $alignLeft = true;

                                                   $icon_1 = 'https://marketing-image-production.s3.amazonaws.com/uploads/0276775d84bf7806c8a2129f707d13f4eaddc3a5cf5e525cb118e226d552ddd50dcb8aab62ec46e538c393421c0a311ceb07acf58f406fe3fdc819cd393222ac.png';
                                                   $width_1 = 17;
                                                   $title_1 = $json2["{$lang}"]['cdName'];
                                                   $text_1 = "{{#customerFirstName}} {{customerFirstName}} {{customerLastName}} {{/customerFirstName}}";

                                                   $icon_2 = 'https://marketing-image-production.s3.amazonaws.com/uploads/0276775d84bf7806c8a2129f707d13f4eaddc3a5cf5e525cb118e226d552ddd50dcb8aab62ec46e538c393421c0a311ceb07acf58f406fe3fdc819cd393222ac.png';
                                                   $width_2 = 17;
                                                   $title_2 = $json2["{$lang}"]['cdDOB'];
                                                   $text_2 = "{{#dateOfBirth}} {{dateOfBirth}} {{/dateOfBirth}}";

                                                   $icon_3 = 'https://marketing-image-production.s3.amazonaws.com/uploads/0276775d84bf7806c8a2129f707d13f4eaddc3a5cf5e525cb118e226d552ddd50dcb8aab62ec46e538c393421c0a311ceb07acf58f406fe3fdc819cd393222ac.png';
                                                   $width_3 = 17;
                                                   $title_3 = $json2["{$lang}"]['cdAge'];
                                                   $text_3 = "{{#age}} {{age}} {{/age}}";

                                                   $icon_4 = 'https://marketing-image-production.s3.amazonaws.com/uploads/4c06f823b6882264d757e23b399ceac76cb3956f55b26a54bac30f1b8a5381d1f0daeadb9b9f5ec139992b5b090a49f9e0bd2a49aa302531d8fde32b6b810164.png';
                                                   $width_4 = 18;
                                                   $title_4 = $json2["{$lang}"]['cdEmail'];
                                                   $text_4 = "{{#email}} {{email}} {{/email}}";

                                                   $icon_5 = 'https://marketing-image-production.s3.amazonaws.com/uploads/a25c94ee6193830bc1321de2196f454d0dc0c2ebb2469f1ded734f2346e54b47d2d3272cbf027055830d8036b07bd2fa8c514bc7a86849688e60b5cffef23520.png';
                                                   $width_5 = 19;
                                                   $title_5 = $json2["{$lang}"]['cdPhone'];
                                                   $text_5 = "{{#phone}} {{phone}} {{/phone}}";

                                                   $icon_6 = 'https://marketing-image-production.s3.amazonaws.com/uploads/b609c1fa19ee9349c48fda0e7d4f21eef39c0643011b974c0a6db6289f3fdd40e377f345a89f193e345b635772c17e1e0ea55e20f33d77192e0fdc3e3e3338c8.png';
                                                   $width_6 = 12;
                                                   $title_6 = $json2["{$lang}"]['cdMobile'];
                                                   $text_6 = "{{#mobilePhone}} {{mobilePhone}} {{/mobilePhone}}";

                                                   $icon_7 = 'https://marketing-image-production.s3.amazonaws.com/uploads/0036710727a5d9b9ca78bdd272be9408c4f7c800990df18013c7f00d3d22ce7fb35b24ce2bb5ac077c0fc2bfededbd4ba78f82c808a543d807bdbb457b96fbea.png';
                                                   $width_7 = 11;
                                                   $title_7 = $json2["{$lang}"]['cdAddress'];
                                                   $text_7 = "{{city}}, {{country}}, {{postcode}}";

                                                   $icon_8 = 'https://marketing-image-production.s3.amazonaws.com/uploads/d28e59f7da1d78671d4f1db525e3ddf6b9d2156d6ea423118a85c75f39e067a1e1cc52031a90b1729800ea13fcd971b49fc9665e60e20c649c3c263e4bc87aec.png';
                                                   $width_8 = 17;
                                                   $title_8 = $json2["{$lang}"]['cdAirline'];
                                                   $text_8 = "{{airline}} {{flightNumber}}";

                                                   $icon_9 = 'https://marketing-image-production.s3.amazonaws.com/uploads/d28e59f7da1d78671d4f1db525e3ddf6b9d2156d6ea423118a85c75f39e067a1e1cc52031a90b1729800ea13fcd971b49fc9665e60e20c649c3c263e4bc87aec.png';
                                                   $width_9 = 17;
                                                   $title_9 = $json2["{$lang}"]['cdPassport'];
                                                   $text_9 = "{{#passportNumber}} {{passportNumber}} {{/passportNumber}}";

                                                   $icon_10 = 'https://marketing-image-production.s3.amazonaws.com/uploads/7d0e221ce0a06e68c971644e8253e1d7eb4565ea84426029ca4d8f1b12d1d261552896d792496d88cf19b2ab1153887660a5d1aaa506222be406a034609cbf89.png';
                                                   $width_10 = 20;
                                                   $title_10 = $json2["{$lang}"]['cdLicence'];
                                                   $text_10 = "{{#drivingLicenseNumber}} {{drivingLicenseNumber}} {{/drivingLicenseNumber}} ";

                                                   include ('../shared/icon-points.php');

                                                   ?>
                                                </td>
                                             </tr>
                                             <tr>
                                                <td align="center" valign="top">
                                                   <table border="0" cellpadding="0" cellspacing="0" class="contentWide-width full-width" style="width: 100%; padding: 0 20px;">
                                                      <tr>
                                                         <td align="center" class="mobile-stack" valign="top">
                                                            <?php

                                                            $buttonLink = '{{baseUrl}}/reservation-management';
                                                            $buttonWidth = 75;
                                                            $buttonText = $json2["{$lang}"]['buttonText2'];
                                                            include ('../shared/button-table.php');
                                                            ?>
                                                         </td>
                                                      </tr>
                                                   </table>
                                                </td>
                                             </tr>
                                          </table>
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                           
                        </table>
                     </td>
                  </tr>
               </table>
               <?php
               include ('../shared/footer.php');
               ?>
            </td>
         </tr>
      </table>
<?php
include ('../shared/page-end.php');
?>