<?php

global $lang;

$lang = basename($_SERVER['PHP_SELF'],'.php');

$data1 = file_get_contents ("../shared/shared.json", FILE_USE_INCLUDE_PATH);
$json1 = json_decode($data1,true);

$data2 = file_get_contents ("translations.json", FILE_USE_INCLUDE_PATH);
$json2 = json_decode($data2,true);
//print_r($json2);

include ('../shared/page-start.php');

?>

      <table align="center" border="0" cellpadding="0" cellspacing="0" class="wrapper-body" width="100%" style="" bgcolor="#e6e6e6">
         <tr>
            <td align="center">
               <?php
               include ('../shared/logo-strip.php');
               ?>
               <table align="center" border="0" cellpadding="0" cellspacing="0" class="wrapper-width wrapper-content" style="width: 640px;" bgcolor="#ffffff">
                  <tr>
                     <td>
                        <table border="0" cellpadding="0" cellspacing="0" class="full-width" align="center" style="width: 100%;">
                           <tr>
                              <td align="center" valign="top">
                                 <table border="0" cellpadding="0" cellspacing="0" class="contentWide-width full-width" style="width: 100%; padding: 0 20px;">
                                    <tr>
                                       <td align="center" class="mobile-stack" valign="top">
                                          <?php
                                          $icon = true;
                                          $icon_type = 'res-canceled';
                                          $titleText = $json2["{$lang}"]['titleText'];
                                          $noPadding = true;
                                          include ('../shared/h1-title.php');
                                          $titleText = $json2["{$lang}"]['subtitleText'];
                                          include ('../shared/h2-title.php');
                                          ?>
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                           <tr>
                               <td align="center" valign="top">
                                 <table border="0" cellpadding="0" cellspacing="0" class="contentWide-width full-width" style="width: 100%; padding: 0 20px;">
                                    <tr>
                                       <td align="center" class="mobile-stack" valign="top">
                                          <?php
                                          $noPadding = false;
                                          $buttonLink = '{{baseUrl}}/reservation-management';
                                          $buttonWidth = 70;
                                          $buttonText = $json2["{$lang}"]['buttonText'];
                                          include ('../shared/button-table.php');
                                          ?>
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                           <tr>
                              <td align="center" valign="top">
                                 <table border="0" cellpadding="0" cellspacing="0" class="content-width" style="width: 440px;">
                                    <tr>
                                       <td align="left" class="mobile-stack" valign="top">
                                          <?php
                                          $alignLeft = true;
                                          $noPadding = true;
                                          $titleText = $json2["{$lang}"]['rdTitle'];
                                          $paragraphText1 = $json2["{$lang}"]['rdMPRef'] . " {{supplierReservationReference}}";
                                          $paragraphText2 = $json2["{$lang}"]['rdRCRef'] . " {{rentalcarsReservationReference}}";
                                          include ('../shared/h2-title-with-paragraph.php');
                                          ?>
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                           <tr>
                              <td align="center" valign="top">
                                 <table border="0" cellpadding="20" cellspacing="0" class="contentWide-width full-width hr-bottom" style="width: 100%; border-bottom-width: 1px; border-bottom-color: #5d5d5d; border-bottom-style: solid; padding: 0 20px;">
                                    <tr>
                                       <td align="center" class="mobile-stack" valign="top">
                                          <table border="0" cellpadding="0" cellspacing="0" class="content-width" style="width: 440px;">
                                             <tr>
                                                <td align="left" class="mobile-stack" valign="top">
                                                   <?php
                                                   
                                                   $number = 2;
                                                   $introText = false;
                                                   $icon_a_1 = 'https://marketing-image-production.s3.amazonaws.com/uploads/136e65ff950301b6ee63c218368633ca9e286964c87af467fe5aa46995ffed831d34b8cdd688ac422842e49ca2dec83a0f690d1a6f4d6dd7858f8446f7695f5d.png';
                                                   $text_a_1 = $json2["{$lang}"]['rdPickDate'];
                                                   $variable_a_1 = '{{pickUpDateTime}}';
                                                   $icon_b_1 = 'https://marketing-image-production.s3.amazonaws.com/uploads/e296f76297c3702edd64418c1770815301e8b23fccc19519da9ff4a8f8db336dba938aa5820b18b79d0cc8a56934c9d7d96517f829d52dd840927ea1c041e5c6.png';
                                                   $text_b_1 = $json2["{$lang}"]['rdPickLoc'];
                                                   $variable_b_1 = '{{pickUpLocationName}}';
                                                   $icon_a_2 = 'https://marketing-image-production.s3.amazonaws.com/uploads/136e65ff950301b6ee63c218368633ca9e286964c87af467fe5aa46995ffed831d34b8cdd688ac422842e49ca2dec83a0f690d1a6f4d6dd7858f8446f7695f5d.png';
                                                   $text_a_2 = $json2["{$lang}"]['rdDropDate'];
                                                   $variable_a_2 = '{{dropOffDateTime}}';
                                                   $icon_b_2 = 'https://marketing-image-production.s3.amazonaws.com/uploads/e296f76297c3702edd64418c1770815301e8b23fccc19519da9ff4a8f8db336dba938aa5820b18b79d0cc8a56934c9d7d96517f829d52dd840927ea1c041e5c6.png';
                                                   $text_b_2 = $json2["{$lang}"]['rdDropLoc'];
                                                   $variable_b_2 = '{{dropOffLocationName}}';
                                                   include ('../shared/split-icon-points.php');

                                                   $number = 5;
                                                   $titleText = false;
                                                   $introText = false;
                                                   $alignLeft = true;

                                                   $icon_1 = 'https://marketing-image-production.s3.amazonaws.com/uploads/7d0e221ce0a06e68c971644e8253e1d7eb4565ea84426029ca4d8f1b12d1d261552896d792496d88cf19b2ab1153887660a5d1aaa506222be406a034609cbf89.png';
                                                   $width_1 = 20;
                                                   $title_1 = $json2["{$lang}"]['cdCarClass'];
                                                   $text_1 = "{{carClass}}";

                                                   $icon_2 = 'https://marketing-image-production.s3.amazonaws.com/uploads/7d0e221ce0a06e68c971644e8253e1d7eb4565ea84426029ca4d8f1b12d1d261552896d792496d88cf19b2ab1153887660a5d1aaa506222be406a034609cbf89.png';
                                                   $width_2 = 20;
                                                   $title_2 = $json2["{$lang}"]['cdMakeModel'];
                                                   $text_2 = "{{makesAndModels}}";

                                                   $icon_3 = 'https://marketing-image-production.s3.amazonaws.com/uploads/2ff908a4cd8820bd776ecab4bf3fe0ea83d1d19fbcb3ad1b92d93ae7e2b4ebbeac7d27ef93c9e8140cb5569970b699c39cd71135f07dbe01f0944c748a8dde88.png';
                                                   $width_3 = 17;
                                                   $title_3 = $json2["{$lang}"]['cdExtras'];
                                                   $text_3 = "{{bookedExtras}}";

                                                   $icon_4 = 'https://marketing-image-production.s3.amazonaws.com/uploads/12070b830f1baa7b49e394a6b3648a0460d4c796ebdcca928542135dfbf1a9344780a56206174143d4bbc5ad7fbb6f7a5f904e1c2bfe0b3c55800f9ab448fb8b.png';
                                                   $width_4 = 18;
                                                   $title_4 = $json2["{$lang}"]['cdTotalPaid'];
                                                   $text_4 = "<tr><td class='text-normal' style='font-family: Verdana, Arial, sans-serif; font-size: 14px;'>". $json2["{$lang}"]['cdRentalTotal'].": {{salePrice}}</td></tr>"
                                                       ."<tr><td class='text-normal' style='font-family: Verdana, Arial, sans-serif; font-size: 14px;'>".$json2["{$lang}"]['cdExtrasTotal']."</td></tr>"
                                                       ."{{#extrasAmounts}}"
                                                       ."<tr><td class='text-normal' style='font-family: Verdana, Arial, sans-serif; font-size: 14px;padding-left:30px;'>{{.}}</td></tr>"
                                                       ."{{/extrasAmounts}}"
                                                       ."<tr><td class='text-normal' style='font-family: Verdana, Arial, sans-serif; font-size: 14px; font-weight: bold;'>".$json2["{$lang}"]['cdTotal']." {{currencyIso}} {{totalAmounts}}</td></tr>";
                                                   
                                                   $icon_5 = 'https://marketing-image-production.s3.amazonaws.com/uploads/0276775d84bf7806c8a2129f707d13f4eaddc3a5cf5e525cb118e226d552ddd50dcb8aab62ec46e538c393421c0a311ceb07acf58f406fe3fdc819cd393222ac.png';
                                                   $width_5 = 17;
                                                   $title_5 = $json2["{$lang}"]['cdCustomerName'];
                                                   $text_5 = "{{customerFirstName}} {{customerLastName}}";
                                                   
                                                   include ('../shared/icon-points.php');

                                                   ?>
                                                </td>
                                             </tr>
                                             <tr>
                                                <td align="center" valign="top">
                                                   <table border="0" cellpadding="0" cellspacing="0" class="contentWide-width full-width" style="width: 100%; padding: 0 20px;">
                                                      <tr>
                                                         <td align="center" class="mobile-stack" valign="top">
                                                            <?php
                                                            $noPadding = false;
                                                            $buttonLink = '{{baseUrl}}/reservation-management';
                                                            $buttonWidth = 73;
                                                            $buttonText = $json2["{$lang}"]['buttonText2'];
                                                            include ('../shared/button-table.php');
                                                            ?>
                                                         </td>
                                                      </tr>
                                                   </table>
                                                </td>
                                             </tr>
                                          </table>
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                           
                        </table>
                     </td>
                  </tr>
               </table>
               <?php
               include ('../shared/footer.php');
               ?>
            </td>
         </tr>
      </table>
<?php
include ('../shared/page-end.php');
?>