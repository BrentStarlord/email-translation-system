<?php

global $lang;

$lang = basename($_SERVER['PHP_SELF'],'.php');

$data1 = file_get_contents ("../shared/shared.json", FILE_USE_INCLUDE_PATH);
$json1 = json_decode($data1,true);

$data2 = file_get_contents ("translations.json", FILE_USE_INCLUDE_PATH);
$json2 = json_decode($data2,true);

include ('../shared/tableWARE/transactional_header.php');

include ('../shared/tableWARE/container_start.php');

$alignCenter = true;
include ('../shared/tableWARE/610_logo.php');
	
$alignLeft = false;
$icon = true;
$iconType = 'birthday';
$heroColor = "blue";
$titleText = $json2["{$lang}"]['titleText'];
$subtitle = $json2["{$lang}"]['subtitleText'];
include ('../shared/tableWARE/hero_text.php');


include ('../shared/tableWARE/spacer_row_20.php');

$paragraphText = $json2["{$lang}"]['introductionText1'];
include ('../shared/tableWARE/text_block.php');

$paragraphText = $json2["{$lang}"]['introductionText2'];
include ('../shared/tableWARE/text_block.php');

$paragraphText = $json2["{$lang}"]['introductionText3'];
include ('../shared/tableWARE/text_block.php');

$paragraphText = $json2["{$lang}"]['introductionText4'];
include ('../shared/tableWARE/text_block.php');

$paragraphText = $json2["{$lang}"]['introductionText5'];
include ('../shared/tableWARE/text_block.php');

$paragraphText = $json2["{$lang}"]['introductionText6'];
include ('../shared/tableWARE/text_block.php');

$paragraphText = $json2["{$lang}"]['introductionText7'];
include ('../shared/tableWARE/text_block.php');

$h2Color = 'blue';
$paragraphText = $json2["{$lang}"]['introductionText8'];
include ('../shared/tableWARE/h2_block.php');

$hasPadding = true;
$textBold = false;
$btnText = $json2["{$lang}"]['buttonText'];
$btnLink = 'https://marketplace.rentalcars.com/#/login';
$btnTitle = $json2["{$lang}"]['buttonText'];
include ('../shared/tableWARE/cta.php');

include ('../shared/tableWARE/sign_off.php');

include ('../shared/tableWARE/spacer_row_20.php');


include ('../shared/tableWARE/footer.php');

include ('../shared/tableWARE/container_end.php');

?>