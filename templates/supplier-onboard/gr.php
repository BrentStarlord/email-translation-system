<?php

global $lang;

$lang = basename($_SERVER['PHP_SELF'],'.php');

$data1 = file_get_contents ("../shared/shared.json", FILE_USE_INCLUDE_PATH);
$json1 = json_decode($data1,true);

$data2 = file_get_contents ("translations.json", FILE_USE_INCLUDE_PATH);
$json2 = json_decode($data2,true);

include ('../shared/page-start.php');

?>

      <table align="center" border="0" cellpadding="0" cellspacing="0" class="wrapper-body" width="100%" style="" bgcolor="#e6e6e6">
         <tr>
            <td align="center">
               <?php
               include ('../shared/logo-strip.php');
               ?>
               <table align="center" border="0" cellpadding="0" cellspacing="0" class="wrapper-width wrapper-content" style="width: 640px;" bgcolor="#ffffff">
                  <tr>
                     <td>
                        <table border="0" cellpadding="0" cellspacing="0" class="full-width" align="center" style="width: 100%;">
                           <tr>
                              <td align="center" valign="top">
                                 <table border="0" cellpadding="20" cellspacing="0" class="contentWide-width" style="width: 100%; padding: 0 20px;">
                                    <tr>
                                       <td align="center" class="mobile-stack" valign="top">
                                          <?php
                                          $icon = true;
                                          $icon_type = 'smile';
                                          $titleText = $json2["{$lang}"]['titleText'];
                                          $noPadding = false;
                                          include ('../shared/h1-title.php');
                                          
                                          $noPadding = true;
                                          $titleText = $json2["{$lang}"]['subtitleText'];
                                          include ('../shared/h2-title.php');


                                          $noPadding = false;

                                          include ('../shared/salutation.php');

                                          $paragraphText = $json2["{$lang}"]['introductionText1'];
                                          $noPadding = true;
                                          include ('../shared/paragraph.php');

                                          $noPadding = false;
                                          $paragraphText = $json2["{$lang}"]['introductionText2'];
                                          include ('../shared/paragraph.php');
                                          ?>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td>
                                          <?php

                                          $icon = false;
                                          $noPadding = false;
                                          $titleText = $json2["{$lang}"]['wdnTitle'];
                                          include ('../shared/h1-title.php');


                                          $number = 2;
                                          $introText = false;
                                          $icon_1 = 'https://marketing-image-production.s3.amazonaws.com/uploads/7e0ee3bf619c5fc0ff82dcfdfd07fe8e13125777a5aa0fc9e83f8c1b3fd8382998fb99ce6e5f477edcf31580fc291223e50c29625a771585207d5c96893aadfd.png';
                                          $width_1 = 63;
                                          $title_1 = $json2["{$lang}"]['wdnAllocationTitle'];
                                          $text_1 = $json2["{$lang}"]['wdnAllocationText'];
                                          $icon_2 = 'https://marketing-image-production.s3.amazonaws.com/uploads/463b16fbd08f212ae957eac3e15cfe93d580ef42affd25bcdcf124a1a77caa3a20f463667939a0687bf92ad68bdffe5e20ce7407260a7e0d3db4999f0d5f1229.png';
                                          $width_2 = 53;
                                          $title_2 = $json2["{$lang}"]['wdnPricingTitle'];
                                          $text_2 = $json2["{$lang}"]['wdnPricingText'];
                                          include ('../shared/icon-area.php');
                                          $noPadding = false;
                                          ?>
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                           <tr>
                              <td align="center" valign="top">
                                 <table border="0" cellpadding="20" cellspacing="0" class="contentWide-width" style="width: 100%; padding: 0 20px;">
                                    <tr>
                                       <td align="center" class="mobile-stack" valign="top">
                                          <?php

                                          $buttonLink = '{{baseUrl}}/login';
                                          $buttonWidth = 63;
                                          $buttonText = $json2["{$lang}"]['buttonText'];
                                          include ('../shared/button-table.php');
                                          ?>
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                           <tr>
                              <td align="center" valign="top">
                                 <table border="0" cellpadding="0" cellspacing="0" class="contentMedium-width hr-top" style="border-top-width: 1px; border-top-color: #5d5d5d; border-top-style: solid; padding: 0 40px;">
                                    <tr>
                                       <td align="center" valign="top" class="mobile-stack text-h2 spacerTop-double content-row content-center" style="font-family: Verdana, Arial, sans-serif; font-size: 24px; border-top-width: 40px; border-top-color: #ffffff; border-top-style: solid; border-bottom-width: 0px; border-bottom-color: #ffffff; border-bottom-style: solid;">
                                          <?php
                                          $noPadding = true;
                                          $titleText = $json2["{$lang}"]['dtdTitle'];
                                          include ('../shared/h2-title.php');
                                          ?>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td align="center" valign="top" class="mobile-stack">
                                          <?php

                                          $number = 2;
                                          $titleText = false;
                                          $introText = false;
                                          $icon_1 = 'https://marketing-image-production.s3.amazonaws.com/uploads/7d0e221ce0a06e68c971644e8253e1d7eb4565ea84426029ca4d8f1b12d1d261552896d792496d88cf19b2ab1153887660a5d1aaa506222be406a034609cbf89.png';
                                          $width_1 = 20;
                                          $title_1 = $json2["{$lang}"]['dtdResTitle'];
                                          $text_1 = $json2["{$lang}"]['dtdResText'];
                                          $icon_2 = 'https://marketing-image-production.s3.amazonaws.com/uploads/12070b830f1baa7b49e394a6b3648a0460d4c796ebdcca928542135dfbf1a9344780a56206174143d4bbc5ad7fbb6f7a5f904e1c2bfe0b3c55800f9ab448fb8b.png';
                                          $width_2 = 17;
                                          $title_2 = $json2["{$lang}"]['dtdWorksTitle'];
                                          $text_2 = $json2["{$lang}"]['dtdWorksText'];
                                          include ('../shared/icon-points.php');
                                          ?>
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                        </table>
                     </td>
                  </tr>
               </table>
               <?php
               include ('../shared/footer.php');
               ?>
            </td>
         </tr>
      </table>
<?php
include ('../shared/page-end.php');
?>