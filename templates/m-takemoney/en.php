<?php

global $lang;

$lang = basename($_SERVER['PHP_SELF'],'.php');

$data1 = file_get_contents ("../shared/shared.json", FILE_USE_INCLUDE_PATH);
$json1 = json_decode($data1,true);

$data2 = file_get_contents ("translations.json", FILE_USE_INCLUDE_PATH);
$json2 = json_decode($data2,true);



include ('../shared/tableWARE/transactional_header.php');

include ('../shared/tableWARE/container_start.php');
$alignCenter = true;
include ('../shared/tableWARE/610_logo.php');

$icon = true;
$heroColor = 'orange';
$iconType = 'res-amended';
$titleText = $json2["{$lang}"]['titleText'];
$subtitle = false;
include ('../shared/tableWARE/hero_text.php');

$paragraphText = $json2["{$lang}"]['introductionText1'];
include ('../shared/tableWARE/text_block.php');

$alignLeft = false;
$btnColor = 'orange';
$btnText = $json2["{$lang}"]['buttonText'];
$btnLink = 'https://marketplace.rentalcars.com/#/billing-rfp-list';
$btnTitle = $json2["{$lang}"]['buttonText'];
include ('../shared/tableWARE/cta.php');

$paragraphText = $json2["{$lang}"]['introductionText2'];
include ('../shared/tableWARE/text_block.php');

$noUrchin = true;
$linkURL = 'mailto:support@marketplace.rentalcars.com';
$paragraphText = $json2["{$lang}"]['introductionText3'];
include ('../shared/tableWARE/link_block.php');

$linkURL = 'https://rcmarketplace.zendesk.com/hc/en-us/articles/360007354154-How-do-I-collect-a-Payment';
$paragraphText = $json2["{$lang}"]['introductionText4'];
include ('../shared/tableWARE/link_block.php');

include ('../shared/tableWARE/sign_off.php');

include ('../shared/tableWARE/spacer_row_20.php');


include ('../shared/tableWARE/footer.php');

include ('../shared/tableWARE/container_end.php');

?>