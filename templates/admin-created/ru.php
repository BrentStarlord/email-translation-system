<?php

global $lang;

$lang = basename($_SERVER['PHP_SELF'],'.php');

$data1 = file_get_contents ("../shared/shared.json", FILE_USE_INCLUDE_PATH);
$json1 = json_decode($data1,true);

$data2 = file_get_contents ("translations.json", FILE_USE_INCLUDE_PATH);
$json2 = json_decode($data2,true);
//print_r($json2);

include ('../shared/page-start.php');

?>

      <table align="center" border="0" cellpadding="0" cellspacing="0" class="wrapper-body" width="100%" style="" bgcolor="#e6e6e6">
         <tr>
            <td align="center">
               <?php
               include ('../shared/logo-strip.php');
               include ('../shared/title-double.php');
               $image = 2;
               include ('../shared/full-image.php');
               ?>
               <table align="center" border="0" cellpadding="0" cellspacing="0" class="wrapper-width wrapper-content" style="width: 640px;" bgcolor="#ffffff">
                  <tr>
                     <td>
                        <table border="0" cellpadding="0" cellspacing="0" class="full-width" align="center" style="width: 100%;">
                           <tr>
                              <td align="center" valign="top">
                                 <table border="0" cellpadding="20" cellspacing="0" class="contentWide-width" style="width: 100%; padding: 0 20px;">
                                    <tr>
                                       <td align="center" class="mobile-stack" valign="top">
                                          <?php
                                          $noPadding = true;
                                          include ('../shared/salutation.php');

                                          $noPadding = false;
                                          $paragraphText = $json2["{$lang}"]['introductionText1'];
                                          include ('../shared/paragraph.php');
                                          ?>
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                           <tr>
                              <td align="center" valign="top">
                                 <table border="0" cellpadding="0" cellspacing="0" class="contentWide-width full-width" style="width: 100%; padding: 0px 20px;">
                                    <tr>
                                       <td align="center" class="mobile-stack" valign="top">
                                          <?php
                                          $buttonLink = '{{baseUrl}}/login/{{activationKey}}';
                                          $buttonWidth = 91;
                                          $buttonText = $json2["{$lang}"]['buttonText'];
                                          include ('../shared/button-table.php');
                                          ?>
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                           <tr>
                              <td align="center" valign="top">
                                 <table border="0" cellpadding="20" cellspacing="0" class="contentWide-width full-width hr-bottom" style="width: 100%; border-bottom-width: 1px; border-bottom-color: #5d5d5d; border-bottom-style: solid; padding: 0 20px;">
                                    <tr>
                                       <td align="center" class="mobile-stack" valign="top">
                                          <?php
                                          $noPadding = true;
                                          $paragraphText = $json2["{$lang}"]['introductionText2'];
                                          include ('../shared/paragraph-bold.php');

                                          $noPadding = false;
                                          $paragraphText = $json2["{$lang}"]['introductionText3'];
                                          include ('../shared/paragraph.php');
                                          
                                          $thanks = true;
                                          include ('../shared/signoff.php');
                                          ?>
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                           <tr>
                              <td align="center" valign="top">
                                 <table border="0" cellpadding="20" cellspacing="0" class="contentWide-width full-width hr-bottom" style="width: 100%; border-bottom-width: 1px; border-bottom-color: #5d5d5d; border-bottom-style: solid; padding: 0 20px;">
                                    <tr>
                                       <td align="center" class="mobile-stack" valign="top">
                                          <?php
                                          $number = 3;
                                          $noPadding = false;
                                          $alignLeft = false;
                                          $titleText = $json2["{$lang}"]['whnTitle'];
                                          $paragraphText = $json2["{$lang}"]['whnIntro'];
                                          $text1 = $json2["{$lang}"]['whnText1'];
                                          $text2 = $json2["{$lang}"]['whnText2'];
                                          $text3 = $json2["{$lang}"]['whnText3'];
                                          include ('../shared/number-points.php');
                                          ?>
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                           <tr>
                              <td align="center" valign="top">
                                 <table border="0" cellpadding="20" cellspacing="0" class="contentWide-width full-width" style="width: 100%; padding: 0 20px;">
                                    <tr>
                                       <td align="center" class="mobile-stack" valign="top">
                                          <?php
                                          $number = 4;
                                          $noPadding = false;
                                          $alignLeft = true;
                                          $titleText = $json2["{$lang}"]['wdTitle'];
                                          $introText = $json2["{$lang}"]['wdIntro'];
                                          $text1 = $json2["{$lang}"]['wdText1'];
                                          $text2 = $json2["{$lang}"]['wdText2'];
                                          $text3 = $json2["{$lang}"]['wdText3'];
                                          $text4 = $json2["{$lang}"]['wdText4'];
                                          include ('../shared/bullet-points.php');

                                          $alignLeft = false;
                                          $noPadding = true;
                                          $paragraphText = $json2["{$lang}"]['wdText5'];
                                          include ('../shared/paragraph.php');

                                          $noPadding = false;
                                          $paragraphText = $json2["{$lang}"]['wdText6'];
                                          $linkURL = '{{baseUrl}}/tnc';
                                          include ('../shared/paragraph-link.php');
                                          ?>
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                        </table>
                     </td>
                  </tr>
               </table>
               <?php
               include ('../shared/footer.php');
               ?>
            </td>
         </tr>
      </table>
<?php
include ('../shared/page-end.php');
?>