<?php

global $lang;

$lang = basename($_SERVER['PHP_SELF'],'.php');

$data1 = file_get_contents ("../shared/shared.json", FILE_USE_INCLUDE_PATH);
$json1 = json_decode($data1,true);

$data2 = file_get_contents ("translations.json", FILE_USE_INCLUDE_PATH);
$json2 = json_decode($data2,true);

include ('../shared/page-start.php');

?>

      <table align="center" border="0" cellpadding="0" cellspacing="0" class="wrapper-body" width="100%" style="" bgcolor="#e6e6e6">
         <tr>
            <td align="center">
               <?php
               include ('../shared/logo-strip.php');
               ?>
               <table align="center" border="0" cellpadding="0" cellspacing="0" class="wrapper-width wrapper-content" style="width: 640px;" bgcolor="#ffffff">
                  <tr>
                     <td>
                        <table border="0" cellpadding="0" cellspacing="0" class="full-width" align="center" style="width: 100%;">
                           <tr>
                              <td align="center" valign="top">
                                 <table border="0" cellpadding="20" cellspacing="0" class="contentWide-width full-width hr-bottom" style="width: 100%; border-bottom-width: 1px; border-bottom-color: #5d5d5d; border-bottom-style: solid; padding: 0 20px;">
                                    <tr>
                                       <td align="center" class="mobile-stack" valign="top">
                                          <?php
                                          $icon = true;
                                          $icon_type = 'graph';
                                          $titleText = '';
                                          $noPadding = true;
                                          include ('../shared/h1-title.php');
                                          ?>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td align="center" class="mobile-stack" valign="top">
                                          <?php

                                          include ('../shared/salutation.php');

                                          $alignLeft = true;
                                          $paragraphText = $json2["{$lang}"]['introductionText1'];
                                          $noPadding = false;
                                          include ('../shared/paragraph.php');

                                          $noPadding = false;
                                          $paragraphText = $json2["{$lang}"]['introductionText2'];
                                          include ('../shared/paragraph.php');

                                          $noPadding = false;
                                          $paragraphText = $json2["{$lang}"]['introductionText3'];
                                          include ('../shared/paragraph.php');
                                          ?>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td>
                                          <?php
                                          $number = 4;
                                          $noPadding = true;
                                          $alignLeft = true;
                                          $text1 = $json2["{$lang}"]['bpoint1'];
                                          $text2 = $json2["{$lang}"]['bpoint2'];
                                          $text3 = $json2["{$lang}"]['bpoint3'];
                                          $text4 = $json2["{$lang}"]['bpoint4'];
                                          include ('../shared/bullet-points.php');
                                          ?>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td>
                                          <?php

                                          $noPadding = false;
                                          $paragraphText = $json2["{$lang}"]['introductionText5'];
                                          include ('../shared/paragraph.php');
                                          ?>
                                       </td>
                                    </tr>

                                    <tr>
                                       <td align="center" class="mobile-stack" valign="top">
                                          <?php

                                          $buttonLink = 'https://marketplace.rentalcars.com/?utm_source=email&utm_medium=button&utm_campaign=discount#/login';
                                          $buttonWidth = 76;
                                          $noPadding = false;
                                          $buttonText = $json2["{$lang}"]['buttonText'];
                                          include ('../shared/button-table.php');
                                          ?>
                                       </td>
                                    </tr>
                                 </table>
                                 <table border="0" cellpadding="20" cellspacing="0" class="contentWide-width full-width" style="width: 100%; padding: 0 20px;">
                                    <tr>
                                       <td align="center" valign="top">
                                          <table border="0" cellpadding=" 0" cellspacing="0" class="contentWide-width full-width" style="width: 100%; padding: 0 20px;">

                                             <tr>
                                                <td align="center" class="mobile-stack" valign="top">
                                                   <?php
                                                   $icon = false;
                                                   $titleText = $json2["{$lang}"]['helpTitle'];
                                                   $noPadding = false;
                                                   include ('../shared/h2-title.php');
                                                   ?>
                                                </td>
                                             </tr>
                                             <tr>
                                                <td>
                                                   <?php
                                                   $noPadding = false;
                                                   $paragraphText = $json2["{$lang}"]['helpText'];
                                                   include ('../shared/paragraph.php');
                                                   ?>
                                                </td>
                                             </tr>

                                             <tr>
                                                <td align="center" class="mobile-stack" valign="top">
                                                   <?php

                                                   $buttonLink = 'https://secure.rentalcars.com/marketplace/en/help?utm_source=email&utm_medium=button&utm_campaign=discount';
                                                   $buttonWidth = 71;
                                                   $noPadding = false;
                                                   $buttonText = $json2["{$lang}"]['buttonText2'];
                                                   include ('../shared/button-table.php');
                                                   ?>
                                                </td>
                                             </tr>
                                             <tr>
                                                <td align="center" class="mobile-stack" valign="top">
                                                   <?php
                                                   $noPadding = true;
                                                   $thanks = true;
                                                   include ('../shared/signoff.php');
                                                   ?>
                                                </td>
                                             </tr>
                                          </table>
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                           
                        </table>
                     </td>
                  </tr>
               </table>
               <?php
               include ('../shared/footer.php');
               ?>
            </td>
         </tr>
      </table>
<?php
include ('../shared/page-end.php');
?>