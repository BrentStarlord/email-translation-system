<?php

global $lang;

$lang = basename($_SERVER['PHP_SELF'],'.php');

$data1 = file_get_contents ("../shared/shared.json", FILE_USE_INCLUDE_PATH);
$json1 = json_decode($data1,true);

$data2 = file_get_contents ("translations.json", FILE_USE_INCLUDE_PATH);
$json2 = json_decode($data2,true);

include ('../shared/tableWARE/transactional_header.php');

include ('../shared/tableWARE/container_start.php');

	include ('../shared/tableWARE/610_logo.php');
	
	$alignLeft = false;
	$titleText = $json2["{$lang}"]['titleText'];
	$subtitle = true;
	$subtitleText = $json2["{$lang}"]['subtitleText'];
	include ('../shared/tableWARE/hero_text.php');


	include ('../shared/tableWARE/spacer_row_20.php');

	$alignLeft = true;
	$textBold = true;
	$paragraphText = $json2["{$lang}"]['introductionText1'];
	include ('../shared/tableWARE/text_block.php');


	$textBold = false;
	$paragraphText = $json2["{$lang}"]['introductionText2'];
	include ('../shared/tableWARE/text_block.php');


	$textBold = false;
	$paragraphText = $json2["{$lang}"]['introductionText3'];
	include ('../shared/tableWARE/h2_block.php');
	
	$number = 3;
	$link_1 = 'https://marketplace.rentalcars.com/#/login';
	$text_1 = $json2["{$lang}"]['bpText1'];

	$link_2 = 'https://marketplace.rentalcars.com/#/login';
	$text_2 = $json2["{$lang}"]['bpText2'];

	$text_3 = $json2["{$lang}"]['bpText3'];

	include ('../shared/tableWARE/bullet-points.php');


	$alignLeft = true;
	$textBold = true;
	$paragraphText = $json2["{$lang}"]['introductionText4'];
	include ('../shared/tableWARE/text_block.php');

	$textBold = false;
	$paragraphText = $json2["{$lang}"]['introductionText5'];
	include ('../shared/tableWARE/text_block.php');
	
	$textBold = false;
	$paragraphText = $json2["{$lang}"]['introductionText6'];
	include ('../shared/tableWARE/text_block.php');
	
	$textBold = false;
	$paragraphText = $json2["{$lang}"]['introductionText7'];
	include ('../shared/tableWARE/link_block.php');
	
	$alignLeft = false;
	$textBold = false;
	$paragraphText = $json2["{$lang}"]['introductionText8'];
	include ('../shared/tableWARE/h2_block.php');
	
	$alignLeft = true;
	$textBold = false;
	$paragraphText = $json2["{$lang}"]['introductionText9'];
	include ('../shared/tableWARE/text_block.php');


	$alignLeft = true;
	include ('../shared/tableWARE/sign_off.php');

	include ('../shared/tableWARE/spacer_row_20.php');


	include ('../shared/tableWARE/footer.php');

include ('../shared/tableWARE/container_end.php');

?>