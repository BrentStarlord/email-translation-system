<?php

global $lang;

$lang = basename($_SERVER['PHP_SELF'],'.php');

$data1 = file_get_contents ("../shared/shared.json", FILE_USE_INCLUDE_PATH);
$json1 = json_decode($data1,true);

$data2 = file_get_contents ("translations.json", FILE_USE_INCLUDE_PATH);
$json2 = json_decode($data2,true);

include ('../shared/tableWARE/transactional_header.php');

include ('../shared/tableWARE/container_start.php');

	include ('../shared/tableWARE/610_logo.php');
	
	$alignLeft = false;
	$titleText = $json2["{$lang}"]['titleText'];
	$subtitle = true;
	$subtitleText = $json2["{$lang}"]['subtitleText'];
	include ('../shared/tableWARE/hero_text.php');


	include ('../shared/tableWARE/spacer_row_20.php');

	$alignLeft = true;
	$paragraphText = $json2["{$lang}"]['introductionText1'];
	include ('../shared/tableWARE/text_block.php');

	$paragraphText = $json2["{$lang}"]['introductionText2'];
	include ('../shared/tableWARE/text_block.php');

	$paragraphText = $json2["{$lang}"]['introductionText3'];
	include ('../shared/tableWARE/text_block.php');

	$listStyle = 'ul';
	include ('../shared/tableWARE/list_start.php');

	$listItemText = $json2["{$lang}"]['listItem1'];
	include ('../shared/tableWARE/list_element.php');

	$listItemText = $json2["{$lang}"]['listItem2'];
	include ('../shared/tableWARE/list_element.php');

	$listItemText = $json2["{$lang}"]['listItem3'];
	include ('../shared/tableWARE/list_element.php');

	$listItemText = $json2["{$lang}"]['listItem4'];
	include ('../shared/tableWARE/list_element.php');

	$listItemText = $json2["{$lang}"]['listItem5'];
	include ('../shared/tableWARE/list_element.php');

	$listItemText = $json2["{$lang}"]['listItem6'];
	$subListNumber = 2;
	$subListItemText_1 = $json2["{$lang}"]['subListItem1'];
	$subListItemText_2 = $json2["{$lang}"]['subListItem2'];
	include ('../shared/tableWARE/list_element.php');

	$subListNumber = 0;
	$listItemText = $json2["{$lang}"]['listItem7'];
	include ('../shared/tableWARE/list_element.php');

	$listItemText = $json2["{$lang}"]['listItem8'];
	include ('../shared/tableWARE/list_element.php');

	$listItemText = $json2["{$lang}"]['listItem9'];
	include ('../shared/tableWARE/list_element.php');

	include ('../shared/tableWARE/list_end.php');


	$paragraphText = $json2["{$lang}"]['introductionText4'];
	include ('../shared/tableWARE/text_block.php');

	$alignLeft = false;
	$btnText = $json2["{$lang}"]['buttonText'];
	$btnLink = 'https://secure.rentalcars.com/marketplace/en/help/commission-and-payments/how-does-the-payment-process-work';
	$btnTitle = $json2["{$lang}"]['buttonTitle'];
	include ('../shared/tableWARE/cta.php');

	$alignLeft = true;
	include ('../shared/tableWARE/sign_off.php');

	include ('../shared/tableWARE/spacer_row_20.php');


	include ('../shared/tableWARE/footer.php');

include ('../shared/tableWARE/container_end.php');

?>