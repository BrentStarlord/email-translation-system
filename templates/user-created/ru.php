<?php

global $lang;

$lang = basename($_SERVER['PHP_SELF'],'.php');

$data1 = file_get_contents ("../shared/shared.json", FILE_USE_INCLUDE_PATH);
$json1 = json_decode($data1,true);

$data2 = file_get_contents ("translations.json", FILE_USE_INCLUDE_PATH);
$json2 = json_decode($data2,true);
//print_r($json2);

include ('../shared/page-start.php');

?>

      <table align="center" border="0" cellpadding="0" cellspacing="0" class="wrapper-body" width="100%" style="" bgcolor="#e6e6e6">
         <tr>
            <td align="center">
               <?php
               include ('../shared/logo-strip.php');
               include ('../shared/title-double.php');
               $image = 2;
               include ('../shared/full-image.php');
               ?>
               <table align="center" border="0" cellpadding="0" cellspacing="0" class="wrapper-width wrapper-content" style="width: 640px;" bgcolor="#ffffff">
                  <tr>
                     <td>
                        <table border="0" cellpadding="0" cellspacing="0" class="full-width" align="center" style="width: 100%;">
                           <tr>
                              <td align="center" valign="top">
                                 <table border="0" cellpadding="20" cellspacing="0" class="contentWide-width full-width" style="width: 100%; border-bottom-width: 1px; border-bottom-color: #ffffff; border-bottom-style: solid; padding: 0 20px;">
                                    <tr>
                                       <td align="center" class="mobile-stack" valign="top">
                                          <?php
                                          $noPadding = true;
                                          include ('../shared/salutation.php');

                                          $noPadding = false;
                                          $paragraphText = $json2["{$lang}"]['introductionText1'];
                                          include ('../shared/paragraph.php');

                                          $buttonLink = '{{baseUrl}}/secure-password/{{activationKey}}';
                                          $buttonWidth = 90;
                                          $buttonText = $json2["{$lang}"]['buttonText'];
                                          include ('../shared/button-table.php');
                                          $paragraphText = $json2["{$lang}"]['introductionText2'];
                                          include ('../shared/paragraph.php');

                                          $seeyou = true;
                                          include ('../shared/signoff.php');
                                          ?>
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                           <tr>
                              <td align="center" valign="top">
                                 <table border="0" cellpadding="20" cellspacing="0" class="contentMedium-width hr-top" style="border-top-width: 1px; border-top-color: #5d5d5d; border-top-style: solid; padding: 0 40px;">
                                    <tr>
                                       <td align="center" valign="top" class="mobile-stack">
                                          <?php

                                          $number = 4;
                                          $titleText = $json2["{$lang}"]['wnTitle'];
                                          $introText = $json2["{$lang}"]['wnIntroText'];
                                          $icon_1 = 'https://marketing-image-production.s3.amazonaws.com/uploads/7d0e221ce0a06e68c971644e8253e1d7eb4565ea84426029ca4d8f1b12d1d261552896d792496d88cf19b2ab1153887660a5d1aaa506222be406a034609cbf89.png';
                                          $width_1 = 20;
                                          $title_1 = $json2["{$lang}"]['wnFleetTitle'];
                                          $text_1 = $json2["{$lang}"]['wnFleetText'];
                                          $icon_2 = 'https://marketing-image-production.s3.amazonaws.com/uploads/12070b830f1baa7b49e394a6b3648a0460d4c796ebdcca928542135dfbf1a9344780a56206174143d4bbc5ad7fbb6f7a5f904e1c2bfe0b3c55800f9ab448fb8b.png';
                                          $width_2 = 17;
                                          $title_2 = $json2["{$lang}"]['wnPricesTitle'];
                                          $text_2 = $json2["{$lang}"]['wnPricesText'];
                                          $icon_3 = 'https://marketing-image-production.s3.amazonaws.com/uploads/274af65a67b754aeacab79b90e40bc95d65a4b989cd4afb355200aa1a180c5c40e4b674e2a39d7c248f06a28c7a15d8f7308d30710354aae8f25ba3d2f3a7b3d.png';
                                          $width_3 = 15;
                                          $title_3 = $json2["{$lang}"]['wnRatesTitle'];
                                          $text_3 = $json2["{$lang}"]['wnRatesText'];
                                          $icon_4 = 'https://marketing-image-production.s3.amazonaws.com/uploads/cff18ca63a0441b819289b14d88f021f1279f6c4c57cae915111cb2d4addab997b965bd494393901762a4a3aa4526c5d7e45a33aaa6776dafe6416535a919de1.png';
                                          $width_4 = 15;
                                          $title_4 = $json2["{$lang}"]['wnAllocationTitle'];
                                          $text_4 = $json2["{$lang}"]['wnAllocationText'];
                                          include ('../shared/icon-points.php');
                                          ?>
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                           
                        </table>
                     </td>
                  </tr>
               </table>
               <?php
               include ('../shared/footer.php');
               ?>
            </td>
         </tr>
      </table>
<?php
include ('../shared/page-end.php');
?>