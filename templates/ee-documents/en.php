<?php

global $lang;

$lang = basename($_SERVER['PHP_SELF'],'.php');

$data1 = file_get_contents ("../shared/shared.json", FILE_USE_INCLUDE_PATH);
$json1 = json_decode($data1,true);

$data2 = file_get_contents ("translations.json", FILE_USE_INCLUDE_PATH);
$json2 = json_decode($data2,true);

include ('../shared/tableWARE/transactional_header.php');

include ('../shared/tableWARE/container_start.php');

	include ('../shared/tableWARE/610_logo.php');
	
	$alignLeft = false;
	$titleText = $json2["{$lang}"]['titleText'];
	$subtitle = true;
	$subtitleText = $json2["{$lang}"]['subtitleText'];
	include ('../shared/tableWARE/hero_text.php');


	include ('../shared/tableWARE/spacer_row_20.php');

	$alignLeft = true;
	$textBold = false;
	$paragraphText = $json2["{$lang}"]['introductionText1'];
	include ('../shared/tableWARE/text_block.php');

	$alignLeft = true;
	$textBold = true;
	$paragraphText = $json2["{$lang}"]['introductionText2'];
	include ('../shared/tableWARE/text_block.php');

	$textBold = false;
	$number = 4;

	$icon_1 = 'https://marketing-image-production.s3.amazonaws.com/uploads/2ff908a4cd8820bd776ecab4bf3fe0ea83d1d19fbcb3ad1b92d93ae7e2b4ebbeac7d27ef93c9e8140cb5569970b699c39cd71135f07dbe01f0944c748a8dde88.png';
	$width_1 = 17;
	$text_1 = $json2["{$lang}"]['pb1Text'];

	$icon_2 = 'https://marketing-image-production.s3.amazonaws.com/uploads/2ff908a4cd8820bd776ecab4bf3fe0ea83d1d19fbcb3ad1b92d93ae7e2b4ebbeac7d27ef93c9e8140cb5569970b699c39cd71135f07dbe01f0944c748a8dde88.png';
	$width_2 = 17;
	$text_2 = $json2["{$lang}"]['pb2Text'];

	$icon_3 = 'https://marketing-image-production.s3.amazonaws.com/uploads/2ff908a4cd8820bd776ecab4bf3fe0ea83d1d19fbcb3ad1b92d93ae7e2b4ebbeac7d27ef93c9e8140cb5569970b699c39cd71135f07dbe01f0944c748a8dde88.png';
	$width_3 = 17;
	$text_3 = $json2["{$lang}"]['pb3Text'];

	$icon_4 = 'https://marketing-image-production.s3.amazonaws.com/uploads/2ff908a4cd8820bd776ecab4bf3fe0ea83d1d19fbcb3ad1b92d93ae7e2b4ebbeac7d27ef93c9e8140cb5569970b699c39cd71135f07dbe01f0944c748a8dde88.png';
	$width_4 = 17;
	$text_4 = $json2["{$lang}"]['pb4Text'];
	include ('../shared/tableWARE/bullet-points-icons.php');

	$textBold = true;
	$alignLeft = true;
	$paragraphText = $json2["{$lang}"]['introductionText3'];
	include ('../shared/tableWARE/text_block.php');

	$alignLeft = false;
	$textBold = false;
	$btnText = $json2["{$lang}"]['buttonText'];
	$btnLink = 'https://marketplace.rentalcars.com/#/login';
	$btnTitle = $json2["{$lang}"]['buttonTitle'];
	include ('../shared/tableWARE/cta.php');

	$alignLeft = true;
	include ('../shared/tableWARE/sign_off.php');

	include ('../shared/tableWARE/spacer_row_20.php');


	include ('../shared/tableWARE/footer.php');

include ('../shared/tableWARE/container_end.php');

?>