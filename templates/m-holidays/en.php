<?php

global $lang;

$lang = basename($_SERVER['PHP_SELF'],'.php');

$data1 = file_get_contents ("../shared/shared.json", FILE_USE_INCLUDE_PATH);
$json1 = json_decode($data1,true);

$data2 = file_get_contents ("translations.json", FILE_USE_INCLUDE_PATH);
$json2 = json_decode($data2,true);

include ('../shared/tableWARE/transactional_header.php');

include ('../shared/tableWARE/container_start.php');

$alignCenter = true;
include ('../shared/tableWARE/610_logo.php');
	
$alignLeft = false;
$icon = true;
$iconType = 'holidays';
$heroColor = "holidays";
$titleText = $json2["{$lang}"]['titleText'];
$subtitleText = $json2["{$lang}"]['subtitleText'];
$subtitle = true;
include ('../shared/tableWARE/hero_text.php');

include ('../shared/tableWARE/spacer_row_20.php');

$alignLeft = true;
$linkURL = 'https://www.rentalcars.com/';
$paragraphText = $json2["{$lang}"]['introductionText1'];
include ('../shared/tableWARE/link_block.php');

$paragraphText = $json2["{$lang}"]['introductionText2'];
include ('../shared/tableWARE/text_block.php');

$paragraphText = $json2["{$lang}"]['introductionText3'];
include ('../shared/tableWARE/text_block.php');

$paragraphText = $json2["{$lang}"]['introductionText4'];
include ('../shared/tableWARE/text_block.php');

$paragraphText = $json2["{$lang}"]['introductionText5'];
include ('../shared/tableWARE/text_block.php');


$textBold = false;
$number = 5;

$icon_1 = 'https://rcmp-marketing-prod.s3.amazonaws.com/images/email/calendar-t.png';
$width_1 = 17;
$text_1 = $json2["{$lang}"]['bp1'];

$icon_2 = 'https://rcmp-marketing-prod.s3.amazonaws.com/images/email/calendar-t.png';
$width_2 = 17;
$text_2 = $json2["{$lang}"]['bp2'];

$icon_3 = 'https://rcmp-marketing-prod.s3.amazonaws.com/images/email/calendar-t.png';
$width_3 = 17;
$text_3 = $json2["{$lang}"]['bp3'];

$icon_4 = 'https://rcmp-marketing-prod.s3.amazonaws.com/images/email/calendar-t.png';
$width_4 = 17;
$text_4 = $json2["{$lang}"]['bp4'];

$icon_5 = 'https://rcmp-marketing-prod.s3.amazonaws.com/images/email/calendar-t.png';
$width_5 = 17;
$text_5 = $json2["{$lang}"]['bp5'];
include ('../shared/tableWARE/bullet-points-icons.php');

include ('../shared/tableWARE/spacer_row_20.php');

$alignLeft = false;
$hasPadding = true;
$ctaColor = 'green';
$textBold = false;
$noUrchin = false;
$btnText = $json2["{$lang}"]['btnText'];
$btnLink = 'https://marketplace.rentalcars.com/#/login';
$btnTitle = $json2["{$lang}"]['btnText'];
include ('../shared/tableWARE/cta.php');


include ('../shared/tableWARE/sign_off.php');

include ('../shared/tableWARE/spacer_row_20.php');


include ('../shared/tableWARE/footer.php');

include ('../shared/tableWARE/container_end.php');

?>