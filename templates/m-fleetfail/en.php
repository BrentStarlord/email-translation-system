<?php

global $lang;

$lang = basename($_SERVER['PHP_SELF'],'.php');

$data1 = file_get_contents ("../shared/shared.json", FILE_USE_INCLUDE_PATH);
$json1 = json_decode($data1,true);

$data2 = file_get_contents ("translations.json", FILE_USE_INCLUDE_PATH);
$json2 = json_decode($data2,true);



include ('../shared/tableWARE/transactional_header.php');

include ('../shared/tableWARE/container_start.php');

   include ('../shared/tableWARE/610_logo.php');
   

   $alignLeft = false;
   $icon = true;
   $iconType = 'res-amended';
   $titleText = $json2["{$lang}"]['titleText'];
   $subtitle = false;
   include ('../shared/tableWARE/hero_text.php');

   $alignLeft = true;

   include ('../shared/tableWARE/salutation.php');

   $paragraphText = $json2["{$lang}"]['introductionText1'];
   include ('../shared/tableWARE/text_block.php');

   $paragraphText = $json2["{$lang}"]['introductionText2'];
   include ('../shared/tableWARE/text_block.php');

   $paragraphText = $json2["{$lang}"]['introductionText3'];
   include ('../shared/tableWARE/text_block.php');



   include ('../shared/tableWARE/spacer_row_20.php');

   $alignLeft = false;
   $btnText = $json2["{$lang}"]['buttonText'];
   $btnLink = 'https://marketplace.rentalcars.com/#/login';
   $btnTitle = $json2["{$lang}"]['buttonText'];
   include ('../shared/tableWARE/cta.php');

   include ('../shared/tableWARE/spacer_row_40.php');

   $alignLeft = true;

   $textBold = true;
   $paragraphText = $json2["{$lang}"]['introductionText4'];
   include ('../shared/tableWARE/text_block.php');


   $textBold = false;
   $paragraphText = $json2["{$lang}"]['introductionText5'];
   include ('../shared/tableWARE/text_block.php');

   $paragraphText = $json2["{$lang}"]['introductionText6'];
   include ('../shared/tableWARE/text_block.php');

   $paragraphText = $json2["{$lang}"]['introductionText7'];
   include ('../shared/tableWARE/text_block.php');

   include ('../shared/tableWARE/sign_off.php');

   include ('../shared/tableWARE/spacer_row_20.php');


   include ('../shared/tableWARE/footer.php');

include ('../shared/tableWARE/container_end.php');

?>