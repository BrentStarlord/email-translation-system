<?php

global $lang;

$lang = basename($_SERVER['PHP_SELF'],'.php');

$data1 = file_get_contents ("../shared/shared.json", FILE_USE_INCLUDE_PATH);
$json1 = json_decode($data1,true);

$data2 = file_get_contents ("translations.json", FILE_USE_INCLUDE_PATH);
$json2 = json_decode($data2,true);



include ('../shared/tableWARE/transactional_header.php');

include ('../shared/tableWARE/container_start.php');
$alignCenter = false;
include ('../shared/tableWARE/610_logo.php');


$alignLeft = false;
$icon = true;
$heroColor = 'lightblue';
$iconType = 'add-location';
$titleText = $json2["{$lang}"]['titleText'];
$subtitleText = $json2["{$lang}"]['subtitleText'];
include ('../shared/tableWARE/hero_text.php');


$alignLeft = true;
include ('../shared/tableWARE/spacer_row_20.php');
$paragraphText = $json2["{$lang}"]['introductionText2'];
include ('../shared/tableWARE/text_block.php');

include ('../shared/tableWARE/spacer_row_20.php');
$textBold = true;
$paragraphText = $json2["{$lang}"]['introductionText3'];
include ('../shared/tableWARE/text_block.php');

include ('../shared/tableWARE/spacer_row_20.php');
$textBold = false;
$linkURL = 'http://marketplace.rentalcars.com/#/dashboard';
$paragraphText = $json2["{$lang}"]['introductionText4'];
include ('../shared/tableWARE/link_block.php');

include ('../shared/tableWARE/spacer_row_20.php');

$paragraphText = $json2["{$lang}"]['introductionText5'];
include ('../shared/tableWARE/h2_block.php');

$noUrchin = false;
$noUrchin = true;
$linkURL = 'mailto:support@marketplace.rentalcars.com';
$paragraphText = $json2["{$lang}"]['introductionText6'];
include ('../shared/tableWARE/link_block.php');

$alignLeft = false;
$btnColor = 'blue';
$btnText = $json2["{$lang}"]['buttonText'];
$noUrchin = true;
$btnLink = 'https://rcmp-marketing-prod.s3.amazonaws.com/images/email/location-document/new-location-form.xlsx';
$btnTitle = $json2["{$lang}"]['buttonText'];
include ('../shared/tableWARE/cta.php');

$alignLeft = true;
include ('../shared/tableWARE/spacer_row_40.php');

$number = 3;
$noPadding = false;
$bulletTitle = $json2["{$lang}"]['introductionText7'];
$bulletIntro = $json2["{$lang}"]['introductionText8'];
$text_1 = $json2["{$lang}"]['bp1'];
$text_2 = $json2["{$lang}"]['bp2'];
$text_3 = $json2["{$lang}"]['bp3'];
include ('../shared/tableWARE/bullet-points.php');

$paragraphText = $json2["{$lang}"]['introductionText9'];
include ('../shared/tableWARE/text_block.php');

include ('../shared/tableWARE/spacer_row_40.php');


$paragraphText = $json2["{$lang}"]['introductionText10'];
include ('../shared/tableWARE/h2_block.php');


$textBold = false;
$noUrchin = false;
$linkURL = 'https://marketplace.rentalcars.com/#/fleet-bau';
$paragraphText = $json2["{$lang}"]['introductionText11'];
include ('../shared/tableWARE/link_block.php');


include ('../shared/tableWARE/sign_off.php');

include ('../shared/tableWARE/spacer_row_20.php');


include ('../shared/tableWARE/footer.php');

include ('../shared/tableWARE/container_end.php');

?>