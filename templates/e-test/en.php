<?php

global $lang;

$lang = basename($_SERVER['PHP_SELF'],'.php');

$data1 = file_get_contents ("../shared/shared.json", FILE_USE_INCLUDE_PATH);
$json1 = json_decode($data1,true);

$data2 = file_get_contents ("translations.json", FILE_USE_INCLUDE_PATH);
$json2 = json_decode($data2,true);

include ('../shared/page-start.php');

?>

      <table align="center" border="0" cellpadding="0" cellspacing="0" class="wrapper-body" width="100%" style="" bgcolor="#e6e6e6">
         <tr>
            <td align="center">
               <?php
               include ('../shared/logo-strip.php');
               ?>
               <table align="center" border="0" cellpadding="0" cellspacing="0" class="wrapper-width wrapper-content" style="width: 640px;" bgcolor="#ffffff">
                  <tr>
                     <td>
                        <table border="0" cellpadding="0" cellspacing="0" class="full-width" align="center" style="width: 100%;">
                           <tr>
                              <td align="center" valign="top">
                                 <table border="0" cellpadding="20" cellspacing="0" class="contentWide-width" style="width: 100%; padding: 0 20px;">
                                    <tr>
                                       <td align="center" class="mobile-stack" valign="top">
                                          <?php
                                          $icon = true;
                                          $icon_type = 'res-amended';
                                          $titleText = $json2["{$lang}"]['titleText'];
                                          $noPadding = false;
                                          include ('../shared/h1-title.php');
                                          ?>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td align="center" class="mobile-stack" valign="top">
                                          <?php

                                          include ('../shared/salutation.php');

                                          $paragraphText = $json2["{$lang}"]['introductionText1'];
                                          $noPadding = false;
                                          include ('../shared/paragraph.php');

                                          $noPadding = false;
                                          $paragraphText = $json2["{$lang}"]['introductionText2'];
                                          include ('../shared/paragraph.php');

                                          $noPadding = false;
                                          $paragraphText = $json2["{$lang}"]['introductionText3'];
                                          include ('../shared/paragraph.php');
                                          ?>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td align="center" class="mobile-stack" valign="top">
                                          <?php

                                          $buttonLink = 'https://marketplace.rentalcars.com/#/login';
                                          $buttonWidth = 84;
                                          $noPadding = true;
                                          $buttonText = $json2["{$lang}"]['buttonText'];
                                          include ('../shared/button-table.php');
                                          ?>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td>
                                          <?php

                                          $noPadding = false;
                                          $titleText = $json2["{$lang}"]['introductionText4'];
                                          include ('../shared/h2-title.php');

                                          $noPadding = false;
                                          $paragraphText = $json2["{$lang}"]['introductionText5'];
                                          include ('../shared/paragraph.php');

                                          $noPadding = false;
                                          $paragraphText = $json2["{$lang}"]['introductionText6'];
                                          include ('../shared/paragraph-bold.php');

                                          $noPadding = true;
                                          $paragraphText = $json2["{$lang}"]['introductionText7'];
                                          include ('../shared/paragraph.php');
                                          ?>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td align="center" valign="top">
                                          <table border="0" cellpadding=" 0" cellspacing="0" class="contentWide-width full-width" style="width: 100%; padding: 0 20px;">
                                             <tr>
                                                <td align="center" class="mobile-stack" valign="top">
                                                   <?php
                                                   $noPadding = true;
                                                   $thanks = true;
                                                   include ('../shared/signoff.php');
                                                   ?>
                                                </td>
                                             </tr>
                                          </table>
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                           
                        </table>
                     </td>
                  </tr>
               </table>
               <?php
               include ('../shared/footer.php');
               ?>
            </td>
         </tr>
      </table>
<?php
include ('../shared/page-end.php');
?>