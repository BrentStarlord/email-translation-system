var browserSync = require('browser-sync'),
    gulp = require('gulp'),
    connect = require('gulp-connect-php'),
    emailBuilder = require('gulp-email-builder'),
    htmlmin = require('gulp-html-minifier'),
    staticPHP = require('gulp-php2html'),
    prompt = require('gulp-prompt'),
    rename = require('gulp-rename'),
    sass = require('gulp-sass'),
    runSequence = require('run-sequence')
    merge = require('merge-stream'),
    os = require('os'),
    fs = require('fs'),
    path = require('path'),
    child_process = require('child_process'),
    run = require('gulp-run'),
    rev = require('gulp-rev'),
    sgMail = require('@sendgrid/mail'),
    es = require("event-stream"),
    exec = require('gulp-exec'),
    log = require('fancy-log'),
    watch = require('gulp-watch'),
    compressCSS = require('gulp-css-format-oneline'),
    slack = require('gulp-slack')({
        url: 'https://hooks.slack.com/services/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',
        channel: '#email-processing', // Optional 
        user: 'Email Processor Bot', // Optional 
        icon_emoji: ':robot_face:' // Optional 
    });

// CONFIGS
var config = {
  projectName: 'WireframeTest',
  brandPath: './brand',
  resourcePath: './resources',
  exportPath: './export',
  sendgridPath: './sendgrid',
  EOAPath: './eoacid'
}

var msg = '';
var emailName = '';
var emailLang = '';

var reload = browserSync.reload;

var sglive = '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';
var sgtest = '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';

// TASKS
gulp.task('connect', function() {
  return connect.server({
    port: '8022',
    hostname: '127.0.0.1',
    base: './emails'
  }, function (){
    browserSync({
      proxy: '127.0.0.1:8022',
      port: '8023',
      startPath: '/'+emailName+'/'+emailLang+'.html'
    });
  });
});



gulp.task('build', function(cb){
  runSequence('sassCompile', 'buildEmail', 'connect', 'watch', cb);
});

gulp.task('watch', function(){
    // watches?
  gulp.watch('**/*.php', ['rebuildEmail', 'reload']);
  gulp.watch('**/*.scss', ['sassCompile', 'rebuildEmail', 'reload']);  
  gulp.watch('**/*.json', ['rebuildEmail', 'reload']);
});

gulp.task('sassCompile', function(){
  return gulp.src(['./sass/embed.scss','./sass/inline.scss'])
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./css'));
});

gulp.task('reload', function () {
    browserSync.reload();
});

gulp.task('buildEmail', function(){

 return gulp.src('package.json')
    .pipe(prompt.prompt([{
        type: 'input',
        name: 'emailName',
        message: 'Please confirm email to build:',
        default: 'e-test'
      },
      {
        type: 'input',
        name: 'language',
        message: 'Please confirm language of email:',
        default: 'en'
      }],
      function(response){
        emailName = response.emailName;
        emailLang = response.language;
        log('./templates/' + emailName + '/' + emailLang +  '.php');
        return gulp.src('./templates/' + emailName + '/' + emailLang +  '.php')
        .pipe(staticPHP())
        .pipe(emailBuilder().build())
        .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(compressCSS())
        .pipe(gulp.dest('./emails/' + emailName + '/'));
      })
    );
});

gulp.task('rebuildEmail', function(){
  return gulp.src('./templates/' + emailName + '/' + emailLang +  '.php')
    .pipe(staticPHP())
    .pipe(emailBuilder().build())
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(compressCSS())
    .pipe(gulp.dest('./emails/' + emailName + '/'));
});

gulp.task('buildAll', function(){
  gulp.src(['./templates/**/*.php', '!./templates/shared/*.php'])
    .pipe(staticPHP())
    .pipe(emailBuilder().build())
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(compressCSS())
    .pipe(gulp.dest('./emails'));
});


// SEND EMAIL TO TESTING PLATFORM 
// This is used to send the email off to Email on Acid
gulp.task('test', function (cb) {
  return gulp.src('package.json')
    .pipe(prompt.prompt([{
        type: 'input',
        name: 'emailName',
        message: 'Please confirm email to build:',
        default: 'e-test'
      },
      {
        type: 'input',
        name: 'language',
        message: 'Please confirm language of email:',
        default: 'en'
      }],
      function(response){
        emailName = response.emailName;
        emailLang = response.language;
        log('./emails/' + emailName + '/' + emailLang + '.html');
        msg = {
          to: 'bookinggo.runme.spam.all@previews.emailonacid.com',
          from: 'help@marketplace.rentalcars.com',
          subject: require('./templates/' + response.emailName + '/subject-' + response.language + '.js'),
          html: fs.readFileSync('./emails/' + response.emailName + '/' + response.language + '.html', 'utf8'),
        };

        sgMail.setApiKey(sgtest);
        sgMail.send(msg)

        return gulp.src('./emails/' + emailName + '/' + emailLang + '.html')
          .pipe(gulp.dest(config.EOAPath + '/' + emailName))
          .pipe(slack('The email *' + emailName + ' (' + emailLang + ')* has been sent to http://www.emailonacid.com for testing.'));
      })
    );
});


// SEND EMAIL TO SENDGRID
// This is used to send the email to SendGrid
gulp.task('send', function () {

  gulp.src('./package.json')
    .pipe(prompt.prompt([{
        type: 'input',
        name: 'emailName',
        message: 'Please confirm email to send:',
        default: 'e-test'
      },
      {
        type: 'input',
        name: 'language',
        message: 'Please confirm language of email:',
        default: 'en'
      }],
      function(response){
        log('./emails/' + response.emailName + '/' + response.language + '.html');

        var sendlist = require('./templates/' + response.emailName + '/sendlist.js');
        msg = {

          to: 'help@marketplace.rentalcars.com',
          bcc: sendlist,
          from: 'help@marketplace.rentalcars.com',
          subject: require('./templates/' + response.emailName + '/subject.js'),
          html: fs.readFileSync('./emails/' + response.emailName + '/' + response.language + '.html', 'utf8'),
        };

        gulp.src('./emails/' + response.emailName + '/' + response.language + '.html')
          .pipe(slack('The email *' + response.emailName + ' (' + response.language + ')* has been sent to sender list `[' + sendlist + ']`.'))
          .pipe(exec(sgMail.setApiKey(sglive)))
          .pipe(sgMail.send(msg))
          .pipe(rev())
          .pipe(gulp.dest(config.sendgridPath + '/' + response.emailName));
      })
    );
});

//build out emails from EN base
gulp.task('multiply', function () {
  gulp.src('./package.json')
    .pipe(prompt.prompt([{
        type: 'input',
        name: 'emailName',
        message: 'Please confirm email to multiply:',
        default: 'e-test'
      }],
      function(response){
        var data = ['ar','au','br','de','es','fr','gr','it','nl','nz','ru','se','pt'];
        var streams = [];
        data.forEach(function(name) {
          var stream = gulp.src('./templates/' + response.emailName + '/en.php')
            .pipe(rename(function(path) {
              path.basename = name;
              path.extname = '.php';
            }))
            .pipe(gulp.dest('./templates/' + response.emailName + '/'));
          streams.push(stream);
        });
        return merge(streams);
      })
    );
});


//Tell me what to do
gulp.task('default', function () {
  log("I think you need 'gulp build' or 'gulp test' or maybe 'gulp send'?");
});
