var browserSync = require('browser-sync'),
    gulp = require('gulp'),
    connect = require('gulp-connect-php'),
    emailBuilder = require('gulp-email-builder'),
    htmlmin = require('gulp-html-minifier'),
    staticPHP = require('gulp-php2html'),
    prompt = require('gulp-prompt'),
    rename = require('gulp-rename'),
    merge = require('merge-stream'),
    os = require('os'),
    fs = require('fs'),
    path = require('path'),
    child_process = require('child_process'),
    run = require('gulp-run'),
    rev = require('gulp-rev'),
    sgMail = require('@sendgrid/mail'),
    es = require("event-stream"),
    exec = require('child_process').exec,
    log = require('fancy-log'),
    slack = require('gulp-slack')({
        url: 'https://hooks.slack.com/services/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',
        channel: '#email-processing', // Optional 
        user: 'Email Processor Bot', // Optional 
        icon_emoji: ':robot_face:' // Optional 
    });

// CONFIGS
var config = {
  projectName: 'WireframeTest',
  brandPath: './brand',
  resourcePath: './resources',
  exportPath: './export',
  sendgridPath: './sendgrid',
  EOAPath: './eoacid'
}

//email on acid config
var eoaconfig = {
  emailaddress: 'bookinggo.runme.spam.all@previews.emailonacid.com'
}

var msg = '';

//var sglive = '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';
var sglive = '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';
var sgtest = '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';

// TASKS
gulp.task('connect-sync', function() {
  connect.server({}, function (){
    browserSync({
      proxy: '127.0.0.1:8000'
    });
  });
});



gulp.task('build', function(){

  gulp.src('package.json')
    .pipe(prompt.prompt([{
        type: 'input',
        name: 'emailName',
        message: 'Please confirm email to build:',
        default: 'e-test'
      }],
      function(response){
        log('./templates/' + response.emailName + '/*.php');
        return gulp.src('./templates/' + response.emailName + '/*.php')
        .pipe(staticPHP())
        .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(gulp.dest('./emails/' + response.emailName + '/'));
      })
    );
});

gulp.task('buildAll', function(){
  gulp.src(['./templates/**/*.php', '!./templates/shared/*.php'])
    .pipe(staticPHP())
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('./emails'));
});

gulp.task('default', ['connect-sync'], function () {
  // watches?
  gulp.watch('**/*.php').on('change', function () {
    browserSync.reload();
  });
  
  gulp.watch('**/*.json').on('change', function () {
    browserSync.reload();
  });
});

// SEND EMAIL TO TESTING PLATFORM 
// This is used to send the email off to Email on Acid
gulp.task('test', function (cb) {

  gulp.src('./package.json')
    .pipe(prompt.prompt([{
        type: 'input',
        name: 'emailName',
        message: 'Please confirm email to test:',
        default: 'e-test'
      },
      {
        type: 'input',
        name: 'language',
        message: 'Please confirm language of email:',
        default: 'en'
      }],
      function(response){
        log('./emails/' + response.emailName + '/' + response.language + '.html');

        msg = {
          to: eoaconfig.emailaddress,
          from: 'help@marketplace.rentalcars.com',
          subject: require('./templates/' + response.emailName + '/subject-' + response.language + '.js'),
          html: fs.readFileSync('./emails/' + response.emailName + '/' + response.language + '.html', 'utf8'),
        };

        return gulp.src('./emails/' + response.emailName + '/' + response.language + '.html')
          .pipe(slack('The email *' + response.emailName + ' (' + response.language + ')* has been sent to http://www.emailonacid.com for testing.'))
          .pipe(exec(sgMail.setApiKey(sgtest)))
          .pipe(sgMail.send(msg))
          .pipe(rev())
          .pipe(gulp.dest(config.EOAPath + '/' + response.emailName));
      })
    );
});


// SEND EMAIL TO SENDGRID
// This is used to send the email to SendGrid
gulp.task('send', function () {

  gulp.src('./package.json')
    .pipe(prompt.prompt([{
        type: 'input',
        name: 'emailName',
        message: 'Please confirm email to send:',
        default: 'e-test'
      },
      {
        type: 'input',
        name: 'language',
        message: 'Please confirm language of email:',
        default: 'en'
      }],
      function(response){
        log('./emails/' + response.emailName + '/' + response.language + '.html');

        var sendlist = require('./templates/' + response.emailName + '/sendlist.js');
        msg = {

          to: 'help@marketplace.rentalcars.com',
          bcc: sendlist,
          from: 'help@marketplace.rentalcars.com',
          subject: require('./templates/' + response.emailName + '/subject.js'),
          html: fs.readFileSync('./emails/' + response.emailName + '/' + response.language + '.html', 'utf8'),
        };

        gulp.src('./emails/' + response.emailName + '/' + response.language + '.html')
          .pipe(slack('The email *' + response.emailName + ' (' + response.language + ')* has been sent to sender list `[' + sendlist + ']`.'))
          .pipe(exec(sgMail.setApiKey(sglive)))
          .pipe(sgMail.send(msg))
          .pipe(rev())
          .pipe(gulp.dest(config.sendgridPath + '/' + response.emailName));
      })
    );
});

//build out emails from EN base
gulp.task('multiply', function () {
  gulp.src('./package.json')
    .pipe(prompt.prompt([{
        type: 'input',
        name: 'emailName',
        message: 'Please confirm email to multiply:',
        default: 'e-test'
      }],
      function(response){
        var data = ['ar','au','br','de','es','fr','gr','it','nl','nz','ru','se','pt'];
        var streams = [];
        data.forEach(function(name) {
          var stream = gulp.src('./templates/' + response.emailName + '/en.php')
            .pipe(rename(function(path) {
              path.basename = name;
              path.extname = '.php';
            }))
            .pipe(gulp.dest('./templates/' + response.emailName + '/'));
          streams.push(stream);
        });
        return merge(streams);
      })
    );
});


//build out emails from EN base
gulp.task('multiply', function () {
  log("I think you need 'gulp build' or 'gulp test' or maybe 'gulp send'?");
});
