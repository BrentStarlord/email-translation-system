<?php
/* BUTTON

variables
$alignLeft (true or center)
$paddingNone(no padding)
$padding (normal padding)
$paddingHalf (half padding)
$paddingDouble (double padding)
$paddingClass (class that equates to above padding size)

*/?>
<table align="<?php echo (isset($alignLeft) && $alignLeft == true ? 'left' : 'center'); ?>" width="100%" border="0" cellspacing="0" cellpadding="<?php echo (isset($paddingNone)) ? $paddingNone : ''; echo (isset($padding)) ? $padding : '';  echo (isset($paddingHalf)) ? $paddingHalf : '';  echo (isset($paddingDouble)) ? $paddingDouble : '';?>" class="a-button <?php echo $paddingClass; ?>">
  <tbody>
    <tr>
      <td>
        <table class="a-button-table" border="0" width="100%" border="0" cellspacing="0" align="center">
          <tbody>
            <tr>
              <td class="a-button-td" valign="middle" align="center">
                <a href="<?php echo $a_button_link; ?>" target="_blank" style="a-button-link"><?php echo $a_button_text; ?></a>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
  </tbody>
</table>